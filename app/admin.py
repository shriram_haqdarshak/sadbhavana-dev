from django.contrib import admin
from django.utils.translation import ugettext_lazy
from django.contrib.auth.admin import UserAdmin
from django.contrib.admin import AdminSite
from django.contrib.admin import ModelAdmin

from .models import Quiz, QuizResult, Question, UserQuiz
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import User,Feedback,SuggestedWatch, WhatsNew, AboutUs, Competition, CompetitionTerms
from content.models import Headers
import csv
from django.http import HttpResponse
from import_export import resources
from import_export.admin import ImportExportModelAdmin

# Hide field if user do not have permission
def remove_from_fieldsets(fieldsets, fields):
    for fieldset in fieldsets:
        for field in fields:
            if field in fieldset[1]['fields']:
                new_fields = []
                for new_field in fieldset[1]['fields']:
                    if not new_field in fields:
                        new_fields.append(new_field)
                fieldset[1]['fields'] = tuple(new_fields)
                break


class ExportCsvMixin:
    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = "Export Users"

class CustomQuizAdmin(ModelAdmin):

  list_display = ['id', 'name', 'lang', 'description', 'pub_date', 'is_active']
  list_filter = ('lang','is_active')

  list_per_page = 25

  # Hide field if user do not have permission
  def get_fieldsets(self, request, obj=None):
      fieldsets = super(CustomQuizAdmin, self).get_fieldsets(request, obj)
      if not request.user.is_superuser and request.user.groups.filter(name='publisher').count() == 0:
          remove_from_fieldsets(fieldsets, ('is_active','pub_date',))
      return fieldsets

class QuestionResource(resources.ModelResource):
    class Meta:
        model = Question

class CustomQuestionAdmin(ImportExportModelAdmin):
  list_display = ['id', 'question_text', 'quiz','is_active']
  list_filter = ('quiz',)
  list_per_page = 25

  # Hide field if user do not have permission
  def get_fieldsets(self, request, obj=None):
      fieldsets = super(CustomQuestionAdmin, self).get_fieldsets(request, obj)
      if not request.user.is_superuser and request.user.groups.filter(name='publisher').count() == 0:
          remove_from_fieldsets(fieldsets, ('is_active','pub_date',))
      return fieldsets

class CustomUserQuiz(ModelAdmin): 
  list_display = ['user', 'quiz']
  list_filter = ('quiz','user')


class CustomUserAdmin(UserAdmin, ExportCsvMixin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    list_display = ['first_name', 'last_name', 'dob', 'email', 'mobile', 'state']

    list_per_page = 25
    actions = ["export_as_csv"]

    def has_add_permission(self, request, obj=None):
        return False

class CustomFeedbackAdminPanel(ModelAdmin):
    list_display = ['name', 'mobile', 'email','created_at'] 

    AdminSite.site_title = ugettext_lazy('Sadbhavana')
    AdminSite.site_header = ugettext_lazy('Sadbhavana Administration')

    list_per_page = 25

class SuggestedWatchAdmin(ModelAdmin):
    list_display = ['title', 'video_id', 'pub_date','seq']
    list_per_page = 25

class WhatsNewAdmin(ModelAdmin):
    list_display = ['title', 'thumbnail_file', 'click_url', 'pub_date', 'seq']
    list_per_page = 25

class AboutUsAdmin(ModelAdmin):
    list_display = ['content', 'lang', 'pub_date']
    list_filter = ('lang',)
    list_per_page = 25

class CompetitionAdmin(ModelAdmin):
    list_display = ['name', 'start_date', 'end_date', 'active']
    list_filter = ('name', 'active')
    list_per_page = 25

class CompetitionTermsAdmin(ModelAdmin):
    list_display = ['competition', 'lang', 'body']
    list_filter = ('competition', 'lang')
    list_per_page = 25

admin.site.register(WhatsNew, WhatsNewAdmin)
admin.site.register(AboutUs, AboutUsAdmin)
admin.site.register(Quiz, CustomQuizAdmin)
admin.site.register(Question, CustomQuestionAdmin)
admin.site.register(UserQuiz, CustomUserQuiz)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Feedback, CustomFeedbackAdminPanel)
admin.site.register(SuggestedWatch, SuggestedWatchAdmin)
admin.site.register(Competition, CompetitionAdmin)
admin.site.register(CompetitionTerms, CompetitionTermsAdmin)