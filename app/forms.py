from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Feedback,User_Uploads
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import PasswordResetForm
from django.utils.translation import gettext as _

User = get_user_model()


class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = User
        fields = ('first_name', 'last_name', 'mobile', 'email', 'dob', 'gender', 'state', 'district')


class CustomUserChangeForm(UserChangeForm):

    class Meta(UserChangeForm):
        model = User
        fields = ('first_name', 'last_name', 'email', 'dob', 'gender', 'state')

class FeedbackForm():
     class Meta:
        model = Feedback
        fields = ('mobile')

class EmailValidationOnForgotPassword(PasswordResetForm):

    def clean_email(self):
        email = self.cleaned_data['email']
        if not User.objects.filter(email__iexact=email, is_active=True).exists():
            msg = _("There is no user registered with the specified E-Mail address.")
            self.add_error('email', msg)
        return email

class ApplyForCompetitionForm(forms.ModelForm):
    required_css_class = 'required'
    class Meta:
        model = User_Uploads
        fields = [
        'title',
        'message',
        'uploaded_file'
        ]
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        super(ApplyForCompetitionForm, self).__init__(*args, **kwargs)
        # there's a `fields` property now
        self.fields['uploaded_file'].required = True
        self.fields['title'].required = True
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
