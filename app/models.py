from django.contrib.auth.models import AbstractUser
from django.core.validators import validate_email
import datetime
from django.conf import settings
from django.db import models
from django.utils import timezone
from django_mysql.models import EnumField
from django.conf import settings
from django.utils.html import format_html


class User(AbstractUser):
    district = models.CharField(max_length=30, default="")
    state = models.CharField(max_length=30, default="")
    mobile = models.CharField(max_length=15, blank=True)
    email = models.EmailField(
        max_length=254, blank=True, unique=False, validators=[validate_email])
    dob = models.DateField(blank=True, null=True)
    gender = EnumField(choices=['Male', 'Female', 'Other'])

    def __str__(self):
        return self.first_name + " " + self.last_name


class Quiz(models.Model):
    name = models.CharField(max_length=100)
    lang = EnumField(choices=[
        ('en', 'English'),
        ('hi', 'Hindi'),
        ('pa', 'Punjabi'),
    ], default='en')
    caption = models.CharField(max_length=150, blank=True)
    description = models.CharField(max_length=254)
    image = models.ImageField(upload_to='quiz/',
                              default='quiz/no_image_available.jpg')
    pub_date = models.DateTimeField(blank=True, null=True, verbose_name='date published')
    is_active = models.BooleanField(default=False)
    show_random = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def pub_date_pretty(self):
        return self.pub_date.strftime('%A, %b %e, %Y %I:%M %p')

    class Meta:
        verbose_name_plural = "Quizzes"


class Question(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    question_text = models.CharField(max_length=400, blank=True)
    question_image = models.ImageField(upload_to='quiz/', blank=True)
    option_a = models.CharField(max_length=100)
    option_b = models.CharField(max_length=100)
    option_c = models.CharField(max_length=100)
    option_d = models.CharField(max_length=100)
    answer = EnumField(
        choices=['Option A', 'Option B', 'Option C', 'Option D'])
    score = models.SmallIntegerField(default=0)
    is_active = models.BooleanField(default=False)
    pub_date = models.DateTimeField(blank=True, null=True, verbose_name='date published')
    desclaimer = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.question_text


class UserQuiz(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    score = models.IntegerField(default=0)
    total_score = models.IntegerField(default=0)
    feedback = models.CharField(max_length=200)
    comments = models.CharField(max_length=200)
    pub_date = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name_plural = "User quizzes"


class QuizResult(models.Model):
    user_quiz = models.ForeignKey(UserQuiz, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = EnumField(
        choices=['NA', 'Option A', 'Option B', 'Option C', 'Option D'])

class Feedback(models.Model):
    name = models.CharField(max_length=100, null=True)
    age = models.CharField(max_length=15,  null=True)
    profession = models.CharField(max_length=15,  null=True)
    email = models.EmailField(max_length=254,  null=True)
    mobile = models.CharField(max_length=15, null=False)
    address = models.TextField(null=True)
    about_platform = EnumField(choices=['Workshop', 'Online', 'Friends','Other','Null'], default="Null", blank=True, null=True)
    login_status = EnumField(choices=['Yes', 'No','Null'], blank=True, default="Null", null=True)
    login_status_explaination = models.TextField(null=True)
    liked_section =EnumField(choices=['Read', 'Watch', 'Play','Engage','Null'], default="Null", blank=True, null=True)
    liked_section_explaination = models.TextField(null=True)
    not_liked_section = EnumField(choices=['Read', 'Watch', 'Play','Engage','Null'], default="Null", blank=True, null=True)
    not_liked_section_explaination = models.TextField(null=True)
    about_sadbhavana = models.TextField(null=True)
    returning_status = EnumField(choices=['Yes', 'No','Null'], default="Null", blank=True, null=True)
    share_sadbhavana = EnumField(choices=['Yes', 'No','Null','may_be'], default="Null", blank=True, null=True)
    share_sadbhavana_why = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class SuggestedWatch(models.Model):
    video_id = models.CharField(max_length=100, null=True, help_text='Id of the youtube video Which you want to show.. Example: 1wsd3eerf3')
    title = models.CharField(max_length=100, null=True, help_text='Title for the video Which you want to show')
    seq = models.IntegerField(
        default=0, verbose_name='Sequence #', help_text='Order in which the videos appear in playlist')
    pub_date = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        verbose_name_plural = "Suggested Watch"

class WhatsNew(models.Model):
    title = models.CharField(max_length=255, null=True, help_text='Title which will show only for first content')
    thumbnail_file = models.FileField(upload_to='app/whatsNew/', blank=True, help_text='Image which you want to show')
    click_url = models.CharField(max_length=255, null=True, help_text='When clicked which page should be open')
    lang = EnumField(choices=[
        ('en', 'English'),
        ('hi', 'Hindi'),
        ('pa', 'Punjabi'),
    ], default='en', verbose_name='Language')
    seq = models.IntegerField(
        default=0, verbose_name='Sequence #', help_text='Order in which the videos appear in playlist')
    pub_date = models.DateTimeField(auto_now_add=True, blank=True)
    text = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Whats New"

class AboutUs(models.Model):
    lang = EnumField(choices=[
        ('en', 'English'),
        ('hi', 'Hindi'),
        ('pa', 'Punjabi'),
    ], default='en', verbose_name='Language', help_text='language in which you want to show about us content')
    content = models.TextField(blank=True, verbose_name='Main Text/Body',help_text='About us content in above selected langauge')
    pub_date = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        verbose_name_plural = "About Us"

class User_Uploads(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255, null=True, blank=True)
    message = models.TextField(blank=True, verbose_name='message')
    uploaded_file = models.FileField()
    upload_date = models.DateTimeField(auto_now_add=True, blank=True)

class Competition(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    start_date = models.DateTimeField(blank=True, verbose_name='Start Date')
    end_date = models.DateTimeField(blank=True, verbose_name='End Date')
    intro_file = models.FileField(null=True, blank=True, verbose_name='Intro Video/Image')
    active = models.BooleanField(default=False, verbose_name='Publish')

    def __str__(self):
        return str(self.name)

    def start_date_pretty(self):
        return self.start_date.strftime('%d %B, %Y')

    def end_date_pretty(self):
        return self.end_date.strftime('%d %B, %Y')

class CompetitionTerms(models.Model):
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    lang = EnumField(choices=[
        ('en', 'English'),
        ('hi', 'Hindi'),
        ('pa', 'Punjabi'),
    ], default='en', verbose_name='Language', help_text='In which language this header should display')
    body = models.TextField(blank=True, verbose_name='message')
    terms_file = models.FileField(null=True, blank=True, verbose_name='Terms and Conditions Image')