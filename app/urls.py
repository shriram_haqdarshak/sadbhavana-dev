from django.urls import path, include
from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from app.views.quiz import PlayQuizView, ResultsView, QuizzesView, MyQuizzesView, QuizTermsView

app_name = 'app'

urlpatterns = [
    path('playquiz/<int:id>', PlayQuizView.as_view(), name='playquiz'),
    path('quizzes/<int:id>', QuizzesView.as_view(), name='quizzes'),
    path('myquizzes/', MyQuizzesView.as_view(), name='myquizzes'),
    path('playquiz/results/<int:pk>', ResultsView.as_view(), name='results'),
    path('quizterms/<int:id>', QuizTermsView.as_view(), name='quizterms'),
]