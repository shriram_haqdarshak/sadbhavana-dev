from django.conf import settings
from .models import SuggestedWatch, WhatsNew, AboutUs, Quiz
from django.utils import translation

def get_settings(context):
  return {'GATrackingId': settings.GA_TRACKING_ID}

def get_latest_quizes(request):
  if translation.LANGUAGE_SESSION_KEY in request.session:
    lang_id = request.session[translation.LANGUAGE_SESSION_KEY]
  else:
    lang_id = 'en'

  quizzes = Quiz.objects.raw(""" SELECT id, name, pub_date FROM app_quiz WHERE is_active = True AND lang = '{}' ORDER BY pub_date DESC LIMIT 4""".format(lang_id))
  return {
    'quizzes': quizzes
  }