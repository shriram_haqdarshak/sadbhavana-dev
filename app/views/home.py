from django.contrib.auth.decorators import login_required
from app.forms import CustomUserCreationForm, CustomUserChangeForm, FeedbackForm, ApplyForCompetitionForm
from django.contrib.auth.forms import PasswordChangeForm
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, update_session_auth_hash
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
import mimetypes
from django.contrib.auth import get_user_model
User = get_user_model()
from django.core.mail import EmailMessage
from random import random
from app.tokens import account_activation_token
from django.conf import settings
from app.models import Feedback, SuggestedWatch, WhatsNew, AboutUs, Competition
from content.models import Headers
from engage.models import About
from django.utils import translation
import datetime
from rgf.settings import EMAIL_HOST_USER
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context

# Not in use
def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')

# Edit/update user profile
def profile(request):
    if translation.LANGUAGE_SESSION_KEY in request.session:
      lang_id = request.session[translation.LANGUAGE_SESSION_KEY]
    else:
      lang_id = 'en'

    dob = ""
    if request.method == 'POST':
        form = CustomUserChangeForm(request.POST, instance=request.user)

        if form.is_valid():
            password1 = request.POST['password1']
            password2 = request.POST['password2']
            password = None

            # Add other validations
            if password2 != password1:
                fullname = form.cleaned_data.get('first_name') + " " + form.cleaned_data.get('last_name')
                context = {"message": "Hi " + fullname + ", Passwords are not matching.", 'dob': dob}
                return render(request, 'registration/profile.html', context)
            elif password1 and password1 != "":
                password = password1

            user = User.objects.get(pk=request.user.id)
            user.email = form.cleaned_data.get('email')
            user.last_name = form.cleaned_data.get('last_name')
            user.first_name = form.cleaned_data.get('first_name')
            user.gender = form.cleaned_data.get('gender')
            user.dob = form.cleaned_data.get('dob')
            dob = user.dob.strftime("%Y-%m-%d")
            user.state = form.cleaned_data.get('state')
            if password:
                user.set_password(password)
            user.save()
            if password:
                update_session_auth_hash(request, user)
            fullname = form.cleaned_data.get('first_name') + " " + form.cleaned_data.get('last_name')
            if lang_id == 'pa':
                context = {"message": "ਹਾਇ " + fullname + ", ਤੁਹਾਡੇ ਵੇਰਵੇ ਸਫਲਤਾਪੂਰਵਕ ਸੁਰੱਖਿਅਤ ਕੀਤੇ ਗਏ ਹਨ.", 'dob': dob}
            elif lang_id == 'hi':
                context = {"message": "नमस्ते " + fullname + ", आपका विवरण सफलतापूर्वक सहेजा गया है.", 'dob': dob}
            else:
                context = {"message": "Hi " + fullname + ", Your details are saved successfully.", 'dob': dob}

            return render(request, 'registration/profile.html', context)
    else:
        form = CustomUserChangeForm()
        if request.user.dob:
          dob = request.user.dob.strftime("%Y-%m-%d")
    return render(request, 'registration/profile.html', {'form': form, 'dob': dob})

# Registration of user
def signup(request, ):
    if translation.LANGUAGE_SESSION_KEY in request.session: 
      lang_id = request.session[translation.LANGUAGE_SESSION_KEY]
    else:
      lang_id = 'en'

    if request.method == 'POST':
        print("request.POST", request.POST)
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)

            email = form.cleaned_data.get('email')
            mobile = form.cleaned_data.get('mobile')
            mobileExists = User.objects.filter(mobile= mobile)
            emailExists = User.objects.filter(email= email)
            if mobileExists:
                if lang_id == 'pa':
                    context = {"message": "ਇਹ ਮੋਬਾਈਲ ਪਹਿਲਾਂ ਹੀ ਰਜਿਸਟਰਡ ਹੈ"}
                elif lang_id == 'hi':
                    context = {"message": "यह मोबाइल पहले से पंजीकृत है"}
                else:
                    context = {"message": "This mobile is already registered"}
                return render(request, 'registration/login.html', context)
            elif emailExists:
                if lang_id == 'pa':
                    context = {"message": "ਇਹ ਈਮੇਲ ਪਹਿਲਾਂ ਤੇਂ ਹੀ ਪੰਜੀਕ੍ਰਿਤ ਹੈ"}
                elif lang_id == 'hi':
                    context = {"message": "यह ईमेल पहले से ही पंजीकृत है"}
                else:
                    context = {"message": "This email is already registered"}
                return render(request, 'registration/login.html', context)

            user.username = mobile if mobile else email
            user.save()

            user_id = user.id
            about_instance = About.objects.create(user_id=user_id)
            about_instance.save()

            fullname = form.cleaned_data.get('first_name') + " " + form.cleaned_data.get('last_name')
            if lang_id == 'pa':
                context = {"message": "ਹਾਇ " + fullname + ", ਸਾਈਨ ਅਪ ਕਰਨ ਲਈ ਤੁਹਾਡਾ ਧੰਨਵਾਦ! ਜਾਰੀ ਰੱਖਣ ਲਈ ਕਿਰਪਾ ਕਰਕੇ ਲੌਗਇਨ ਕਰੋ."}
            elif lang_id == 'hi':
                context = {"message": "नमस्ते " + fullname + ", साइन अप करने के लिए आभार! जारी रखने के लिए कृपया लॉग इन करें."}
            else:
                context = {"message": "Hi " + fullname + ", Thank you for signing up! Please login to continue."}
            return render(request, 'registration/login.html', context)
        else:
            return render(request, 'registration/signup.html', {'form': form, 'first_name': request.POST['first_name'], 'last_name': request.POST['last_name'], 'dob': request.POST['dob'], 'gender': request.POST['gender'], 'email': request.POST['email'], 'mobile': request.POST['mobile'], 'state': request.POST['state']})
    else:
        form = CustomUserCreationForm()
    return render(request, 'registration/signup.html', {'GATrackingId': settings.GA_TRACKING_ID, 'form': form})

# Not in use
def generate_otp(request):
    otp = random.randint(1000, 9999)
    message = "Here is your OTP {} for registration \n Don't share it with anyone".format(otp)
    number = request.GET['number']

    return HttpResponse(otp)

# Not in use
import urllib.request
import urllib.parse
def sendSMS(apikey, numbers, sender, message):
    data = urllib.parse.urlencode({'apikey': apikey, 'numbers': numbers,
                                   'message': message, 'sender': sender})
    data = data.encode('utf-8')
    request = urllib.request.Request("https://api.textlocal.in/send/?")
    f = urllib.request.urlopen(request, data)
    fr = f.read()
    return (fr)


#feedback form
def savefeedback(request):
    if request.method == 'POST':
        radio_default_value= dict(request.POST)
        obj = Feedback()
        obj.name = request.POST['name']
        obj.age = request.POST['age']
        obj.profession = request.POST['profession']
        obj.mobile = request.POST['mobile']
        obj.email = request.POST['email']
        obj.address = request.POST['address']
        obj.about_platform = radio_default_value['platform'][0]
        obj.login_status = radio_default_value['logging_in'][0]
        obj.login_status_explaination = request.POST['explain_logging']
        obj.liked_section = radio_default_value['liked_section'][0]
        obj.liked_section_explaination = request.POST['liked_section_explain']
        obj.not_liked_section = radio_default_value['not_liked_section'][0]
        obj.platform_interest = request.POST['platform_interest']
        obj.about_sadbhavana = request.POST['about_sadbhavana']
        obj.returning_status = radio_default_value['return_to_platform'][0]
        obj.share_sadbhavana = radio_default_value['share_sadbhavana'][0]
        obj.share_sadbhavana_why = request.POST['share_sadbhavana_brief']
        obj.save()
        context = {"message": "Feedback Sumbitted Successfully"}
        return render(request, 'feedback.html', context)
    else:
        return render(request, 'feedback.html', {'GATrackingId': settings.GA_TRACKING_ID})

# Search operation
def autocompleteModel(request):
    if translation.LANGUAGE_SESSION_KEY in request.session:
      lang_id = request.session[translation.LANGUAGE_SESSION_KEY]
    else:
      lang_id = 'en'

    q = request.GET.get('tags','')
    search_qs = Headers.objects.filter(tags__icontains = q, lang= lang_id)
    context = {
        'baseURL': settings.BASE_URL,
        "searchResults": search_qs
        }

    return render(request, 'search-results.html', context)


def HomeContent(request):
    if translation.LANGUAGE_SESSION_KEY in request.session: 
      lang_id = request.session[translation.LANGUAGE_SESSION_KEY]
    else:
      lang_id = 'en'

    AboutUsContent = AboutUs.objects.raw(""" SELECT * FROM app_aboutus WHERE lang = '{}' """.format(lang_id))
    WhatsNewContent = WhatsNew.objects.raw(""" SELECT * FROM app_whatsnew ORDER BY seq ASC """)
    SuggestedVideos = SuggestedWatch.objects.raw(""" SELECT id, title, video_id FROM app_suggestedwatch ORDER BY seq DESC """)

    other = """ SELECT *,cs.title as content_heading FROM content_headers ch
        LEFT JOIN content_subcategory cs ON cs.id = ch.sub_category_id
        WHERE ch.category_id = 3 AND ch.sub_category_id = 11 AND ch.active = 1 order by ch.seq desc"""
    otherContent = Headers.objects.raw(other)

    if otherContent :
        otherContentTitle = otherContent[0].content_heading
    else :
        otherContentTitle = ''

    if AboutUsContent:
        AboutUsContent = AboutUsContent[0]
    else:
        AboutUsContent = AboutUsContent

    isCompetitionLive = Competition.objects.raw(""" SELECT * FROM app_competition WHERE start_date <= '{}' AND end_date >= '{}' AND active = 1 """.format(datetime.datetime.now(),datetime.datetime.now()))

    context = {
        'baseURL': settings.BASE_URL,
        'GATrackingId': settings.GA_TRACKING_ID,
        "AboutUsContent": AboutUsContent,
        "WhatsNewContent": WhatsNewContent,
        "SuggestedVideos": SuggestedVideos,
        "otherContent": otherContent,
        "otherContentTitle": otherContentTitle,
        "isCompetitionLive": isCompetitionLive
        }

    return render(request, 'home.html', context)

# save competition entries if completion is going else we save write to us content
@login_required
def applyforcompetition(request):
    form = ApplyForCompetitionForm()

    comp = """ SELECT * FROM app_competition
        WHERE start_date <= '{}' AND end_date >= '{}' AND active = 1""".format(datetime.datetime.now(),datetime.datetime.now())
    compContent = Headers.objects.raw(comp)
    if compContent :
        compName = compContent[0].name
    else :
        compName = "WRITE TO US"

    if request.method == 'POST':
        form = ApplyForCompetitionForm(request.POST, request.FILES)
        if form.is_valid():
            user_pr = form.save(commit=False)
            user_pr.uploaded_file = request.FILES['uploaded_file']
            user_pr.upload_date = datetime.datetime.now()
            user_pr.user_id = request.user.id
            user_pr.title = request.POST['title']
            user_pr.message = request.POST['message']
            file_type = user_pr.uploaded_file.url.split('.')[-1]
            file_type = file_type.lower()

            user_pr.save()

            #send Email
            user = User.objects.get(pk=request.user.id)
            htmly = get_template('apply-for-competition-email.html')
            d = dict({ 'user': user , 'file_name': str(user_pr.uploaded_file), 'title': user_pr.title, 'message': user_pr.message })
            subject, from_email, to = compName, EMAIL_HOST_USER, 'sadbhavanasesamadhaan@gmail.com'
            text_content = 'https://rgf-bucket.s3.ap-south-1.amazonaws.com/'+str(user_pr.uploaded_file)
            html_content = htmly.render(d)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()

            return HttpResponse('Your Response received Successfully')
    context = {"form": form,"compName": compName}
    return render(request, 'apply-for-competition.html', context)

# show competition introduction and terms and conditions
# @login_required
def precompetition(request):
    if translation.LANGUAGE_SESSION_KEY in request.session: 
      lang_id = request.session[translation.LANGUAGE_SESSION_KEY]
    else:
      lang_id = 'en'

    comp = """ SELECT * FROM app_competition ac
        Inner join app_competitionterms act on ac.id=act.competition_id
        WHERE ac.start_date <= '{}' AND ac.end_date >= '{}' AND ac.active = 1 AND act.lang = '{}'""".format(datetime.datetime.now(),datetime.datetime.now(),lang_id)
    compContent = Headers.objects.raw(comp)

    for comp in compContent:
        file_mime = mimetypes.guess_type(str(comp.intro_file))

        if file_mime[0] == 'image/jpeg' or file_mime[0] == 'image/png' :
            mediaFormat = 'image'
        elif file_mime[0] == 'video/mp4' or file_mime[0] == 'video/3gpp' or file_mime[0] == 'video/webm':
            mediaFormat = 'video'
        else:
            mediaFormat = 'unsupported'

        comp.mediaFormat = mediaFormat

    context = {"compContent": compContent[0]}
    return render(request, 'pre-competition.html', context)