from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

from django.views import generic
from app.models import Question, QuizResult, UserQuiz, Quiz
from engage.models import SadbhavanaScore
from django.utils import translation
from django.conf import settings
from django.db.models import F

class PlayQuizView(generic.ListView):
    template_name = 'quiz/playquiz.html'
    model = Question

    # Save quiz result
    def post(self, request, **kwargs):
        data = dict(request.POST)

        res = UserQuiz(user_id=request.user.id, quiz_id=kwargs['id'], comments='')
        res.save()

        score, answers, results, total = 0, [], [], 0
        for q in Question.objects.filter(is_active=True, quiz_id=kwargs['id']):

            _ans = "NA"
            total += q.score
            if "choice"+str(q.id) in data:
                _ans = data["choice"+str(q.id)][0]
                if str(q.answer) == _ans:
                    score += q.score
            results.append(QuizResult(user_quiz_id=res.id, question_id=q.id, answer=_ans))

        QuizResult.objects.bulk_create(results)

        res.score = score

        res.total_score = 10
        res.save()

        # Update sadbhavana score table
        points = score
        if request.user.id is not None :
            objUser = SadbhavanaScore.objects.filter(user_id=request.user.id)

            if objUser:
                SadbhavanaScore.objects.filter(user_id=request.user.id).update(points=F('points')+points)
            else:
                user = SadbhavanaScore(user_id=request.user.id,points=points)
                user.save()

        context = {}  #  set your context

        return HttpResponseRedirect('results/' + str(res.id))

    # get quiz
    # def get_queryset(self):
    def get(self, *args, **kwargs):

        quiz = Quiz.objects.filter(is_active=True, id=kwargs['id'])

        for q in quiz:
            is_random = q.show_random
        if is_random == True:
            questions = Question.objects.all().filter(quiz_id=kwargs['id'], is_active=True).order_by('?')[:10]
        else:
            questions = Question.objects.all().filter(quiz_id=kwargs['id'], is_active=True).order_by('id')[:10]

        context = {'questions': questions, "quiz": quiz[0], 'baseURL': settings.BASE_URL, 'navBar': 'play'}

        return render(self.request, 'quiz/playquiz.html', context)


class QuizzesView(generic.DetailView):
    model = Quiz
    template_name = 'quiz/quizzes.html'

    # Get all quiz
    def get(self, *args, **kwargs):
        if translation.LANGUAGE_SESSION_KEY in self.request.session: 
            lang_id = self.request.session[translation.LANGUAGE_SESSION_KEY]
        else:
            lang_id = 'en'

        quizzes = Quiz.objects.raw(""" SELECT id, name, caption, description, image FROM app_quiz WHERE is_active = True AND lang = '{}' """.format(lang_id))

        quiz_id = kwargs['id']
        metadata = """ SELECT id, image, name, description FROM app_quiz WHERE id = {} """.format(quiz_id)
        metaData = Quiz.objects.raw(metadata)

        context = {
            'quizzes': quizzes,
            'metaData': metaData[0],
            'baseURL': settings.BASE_URL,
            'navBar': 'play'
        }
        return render(self.request, 'quiz/quizzes.html', context)

# Get terms and conditions for quiz
class QuizTermsView(LoginRequiredMixin, generic.DetailView):
    model = Quiz
    template_name = 'quiz/quiz-terms-and-conditions.html'

    def get(self, *args, **kwargs):
        quizzes = Quiz.objects.raw("""SELECT id, name, caption, description, image as image FROM app_quiz WHERE is_active = True AND id = {}""".format(kwargs['id']))

        context = {
            'quizterms': quizzes[0], 
            'baseURL': settings.BASE_URL,
            'navBar': 'play'
        }
        return render(self.request, 'quiz/quiz-terms-and-conditions.html', context)


class MyQuizzesView(LoginRequiredMixin, generic.DetailView):
    model = UserQuiz
    template_name = 'quiz/myquizzes.html'

    # get login user attended quiz
    def get(self, *args, **kwargs):

        q = """ SELECT au.id, aq.name, quiz_id, score, au.pub_date, image as image FROM app_userquiz au inner join app_quiz aq on aq.id=au.quiz_id WHERE user_id = {} order by au.pub_date desc""".format(self.request.user.id)
        results = UserQuiz.objects.raw(q)

        context = {
            'results': results,
            'user': self.request.user, 
            'baseURL': settings.BASE_URL,
            'navBar': 'play'
        }
        return render(self.request, 'quiz/myquizzes.html', context)


class ResultsView(generic.DetailView):
    model = QuizResult
    template_name = 'quiz/quiz-results.html'

    # show result of quiz after finishing quiz
    def get(self, *args, **kwargs):

        result = UserQuiz.objects.filter(pk=kwargs['pk'], user_id=self.request.user.id)
        answers = QuizResult.objects.filter(user_quiz_id=result[0].id)
        quiz = Quiz.objects.filter(id=result[0].quiz_id)

        context = {
            'result': result[0],
            'answers': answers,
            'quiz': quiz[0], 
            'baseURL': settings.BASE_URL,
            'navBar': 'play'
        }
        return render(self.request, 'quiz/quiz-results.html', context)

# not in use
@login_required
def result(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]

    context = {
        'latest_question_list': latest_question_list,
    }

    return render(request, 'quiz/playquiz.html', context)

