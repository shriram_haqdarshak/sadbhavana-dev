from django.conf import settings
from django.utils import translation

class ForceInEnglish:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.path.startswith('/admin'):
            request.LANG = 'en'
            translation.activate(request.LANG)
            request.LANGUAGE_CODE = request.LANG

        return self.get_response(request)