from django.db import models
from django_mysql.models import EnumField
from django.utils import timezone

# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=2500)
    active = models.BooleanField(default=False, verbose_name='Publish')
    pub_date = models.DateTimeField(blank=True, null=True, verbose_name='Date Published')
    seq = models.IntegerField(
        default=0, verbose_name='Sequence #', help_text='Order in which the content to appear on page')

    def __str__(self):
        return self.title

    def pub_date_pretty(self):
        return self.pub_date.strftime('%d %B, %Y')

    class Meta:
        verbose_name_plural = "Categories"

class SubCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    title = models.CharField(max_length=2500)
    active = models.BooleanField(default=False, verbose_name='Publish')
    pub_date = models.DateTimeField(blank=True, null=True, verbose_name='Date Published')
    seq = models.IntegerField(
        default=0, verbose_name='Sequence #', help_text='Order in which the content to appear on page')

    def __str__(self):
        return self.title

    def pub_date_pretty(self):
        return self.pub_date.strftime('%d %B, %Y')

    def get_header_category(self):
        return self.category.title

    class Meta:
        verbose_name_plural = "Sub Categories"

class LanguageLocale(models.Model):
    lang_key = models.CharField(max_length=255, null=True, help_text='Key for Header')
    lang = EnumField(choices=[
        ('en', 'English'),
        ('hi', 'Hindi'),
        ('pa', 'Punjabi'),
    ], default='en', verbose_name='Language')
    translation = models.TextField(blank=True, verbose_name='Main Text/Body', help_text='Header title')

    def __str__(self):
        return self.lang_key

    def get_language_locale(self):
        return self.language_locale.lang_key

    class Meta:
        verbose_name_plural = "Language Locale's"

class Headers(models.Model):
    language_locale = models.ForeignKey(LanguageLocale, on_delete=models.CASCADE, blank=True, null=True, help_text='Select key for Header')
    title = models.CharField(max_length=2500, default='', help_text='Title of the Header')
    lang = EnumField(choices=[
        ('en', 'English'),
        ('hi', 'Hindi'),
        ('pa', 'Punjabi'),
    ], default='en', verbose_name='Language', help_text='In which language this header should display')
    age_min = models.IntegerField(default=1, help_text='Minimum age for which this header should display')
    age_max = models.IntegerField(default=99, help_text='Maximum age for which this header should display')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, help_text='Select in which menu this header should display')
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE, help_text='Select in which tab this header should display')
    content_type = EnumField(
        choices=['Text', 'Audio', 'Video', 'Image', 'PDF'], help_text='Select the type of Header')
    meta_type = EnumField(choices=[('Doc', 'Document'), 'Link'], default='Doc',
                          verbose_name='Document/Link', help_text='Embedded Document or External Link')
    summary = models.TextField(blank=True, verbose_name='Summary/Short Description',
         help_text='This Will appear below title')
    thumbnail_file = models.FileField(upload_to='content/header/', blank=True, help_text='Upload image file')
    document_file = models.FileField(upload_to='content/header/', blank=True, help_text='When content type is PDF, Audio, upload file')
    thumbnail_url = models.URLField(max_length=255, blank=True, help_text='Url of external image')
    iframe_url = models.URLField(max_length=255, blank=True, help_text='Example: https://www.youtube.com/embed/o4vSElkWV7Y')
    reference_website = models.CharField(max_length=2500, blank=True, help_text='Name of the website where header will redirect')
    redirect_url = models.URLField(max_length=255, blank=True, help_text='Url of website to redirect')
    tags = models.CharField(max_length=2500, blank=True,
                            help_text='Keywords/tags separated by comma')
    seq = models.IntegerField(default=0, verbose_name='Sequence #', help_text='Order in which the content to appear on page')
    active = models.BooleanField(default=False, verbose_name='Publish')
    pub_date = models.DateTimeField(blank=True, null=True, verbose_name='Date Published')

    def __str__(self):
        return self.title

    def pub_date_pretty(self):
        return self.pub_date.strftime('%d %B, %Y')

    class Meta:
        verbose_name_plural = "Headers"


def content_directory_path(instance, filename):
    path = 'content'
    if (instance.header.sub_category != ''):
        path += '/{0}'.format(instance.header.sub_category)
    path += '/{0}/{1}'.format(instance.header.title, filename)
    return path


class Details(models.Model):
    language_locale = models.ForeignKey(LanguageLocale, on_delete=models.CASCADE, blank=True, null=True, help_text='Select key for Header')
    header = models.ForeignKey(Headers, on_delete=models.CASCADE, help_text='Select the Header')
    #book = models.ForeignKey(Book, on_delete=models.CASCADE, default='', null=True)
    title = models.CharField(max_length=2500, default='',help_text='Title for the Header')
    lang = EnumField(choices=[
        ('en', 'English'),
        ('hi', 'Hindi'),
        ('pa', 'Punjabi'),
    ], default='en', verbose_name='Language')
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE, blank=True, null=True, help_text='Select sub category')
    tag_line = models.CharField(max_length=8000, blank=True)
    summary = models.TextField(blank=True, verbose_name='Summary/Short Description',
                               help_text='Will appear on Facebook and WhatsApp share messages')
    text = models.TextField(blank=True, verbose_name='Main Text/Body',help_text='Content should be in HTML format')
    url = models.URLField(max_length=255, blank=True,
                          verbose_name='Link URL', help_text='For external links')
    img_file = models.ImageField(
        upload_to=content_directory_path, blank=True, verbose_name='Image/Animation File')
    content_file = models.FileField(
        upload_to=content_directory_path, blank=True, verbose_name='Audio/Video/PDF File')

    def __str__(self):
        return self.title

    def get_header_category(self):
        return self.header.category

    def get_header_sub_category(self):
        return self.header.sub_category

    class Meta:
        verbose_name_plural = "Details/Translations"


class Book(models.Model):
    header = models.ForeignKey(Headers, on_delete=models.CASCADE, default='', help_text='Select Header')
    details = models.ForeignKey(Details, on_delete=models.CASCADE, default='')
    thumbnail_file = models.FileField(upload_to=content_directory_path, blank=True, help_text='Book page')
    seq = models.IntegerField(
        default=0, verbose_name='Sequence #', help_text='Order in which the pages to appear on page')

    class Meta:
        verbose_name_plural = "Book"
