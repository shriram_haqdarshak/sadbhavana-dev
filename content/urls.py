from django.urls import path, include
from . import views

urlpatterns = [
    path('read', views.getread, name='read'),
    path('watch', views.getmedia, name='watch'),
    path('read/speech/<int:header_id>', views.getspeech, name='speech'),
    path('read/biography/<int:header_id>', views.getbiography, name='biography'),
    path('read/books/<int:header_id>', views.getbooks, name='books'),
    path('watch-video/<int:header_id>', views.watchvideo, name='watch-video'),
    path('test', views.gettest, name='test'),
]