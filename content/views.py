from django.shortcuts import render, get_object_or_404
from .models import Headers, Details, Category, SubCategory
from django.conf import settings
from django.utils import translation

# Create your views here.

# Get content of read section
def getread(request):
    if translation.LANGUAGE_SESSION_KEY in request.session:
      lang_id = request.session[translation.LANGUAGE_SESSION_KEY]
    else:
      lang_id = 'en'

    speech = """ SELECT * FROM content_headers ch
        Inner join content_languagelocale cl on cl.id=ch.language_locale_id
        WHERE ch.category_id = 1 AND ch.sub_category_id = 1 AND ch.lang = '{}' AND ch.active = 1 order by ch.seq asc""".format(lang_id)

    biography = """ SELECT * FROM content_headers ch
        Inner join content_languagelocale cl on cl.id=ch.language_locale_id
        WHERE ch.category_id = 1 AND ch.sub_category_id = 2 AND ch.lang = '{}' AND ch.active = 1 order by ch.seq asc""".format(lang_id)

    books = """ SELECT * FROM content_headers ch
        Inner join content_languagelocale cl on cl.id=ch.language_locale_id
        WHERE ch.category_id = 1 AND ch.sub_category_id = 3 AND ch.lang = '{}' AND ch.active = 1 order by ch.seq asc""".format(lang_id)

    resources = """ SELECT * FROM content_headers ch
        Inner join content_languagelocale cl on cl.id=ch.language_locale_id
        WHERE ch.category_id = 1 AND ch.sub_category_id = 4 AND ch.lang = '{}' AND ch.active = 1 order by ch.seq asc""".format(lang_id)

    speechContent = Headers.objects.raw(speech)
    if not speechContent :
      speech = """ SELECT * FROM content_headers ch
        Inner join content_languagelocale cl on cl.id=ch.language_locale_id
        WHERE ch.category_id = 1 AND ch.sub_category_id = 1 AND ch.lang = 'en' AND ch.active = 1 order by ch.seq asc"""
    speechContent = Headers.objects.raw(speech)

    for content in speechContent :
      content.youtube_id = content.iframe_url.rsplit('/', 1)[-1]

    biographyContent = Headers.objects.raw(biography)
    if not biographyContent :
      biography = """ SELECT * FROM content_headers ch
        Inner join content_languagelocale cl on cl.id=ch.language_locale_id
        WHERE ch.category_id = 1 AND ch.sub_category_id = 2 AND ch.lang = 'en' AND ch.active = 1 order by ch.seq asc"""
    biographyContent = Headers.objects.raw(biography)

    booksContent = Headers.objects.raw(books)
    if not booksContent :
      books = """ SELECT * FROM content_headers ch
        Inner join content_languagelocale cl on cl.id=ch.language_locale_id
        WHERE ch.category_id = 1 AND ch.sub_category_id = 3 AND ch.lang = 'en' AND ch.active = 1 order by ch.seq asc"""
    booksContent = Headers.objects.raw(books)

    resourcesContent = Headers.objects.raw(resources)
    if not resourcesContent :
      resources = """ SELECT * FROM content_headers ch
        Inner join content_languagelocale cl on cl.id=ch.language_locale_id
        WHERE ch.category_id = 1 AND ch.sub_category_id = 4 AND ch.lang = 'en' AND ch.active = 1 order by ch.seq asc"""
    resourcesContent = Headers.objects.raw(resources)

    constitution = """ SELECT ch.*, cd.*, cb.*, cd.summary as detailSummary, cb.thumbnail_file as bookimg FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        left join content_book cb on cd.id=cb.details_id
        WHERE ch.category_id = 1 AND ch.sub_category_id = 9 """.format()
    constitutionContent = Headers.objects.raw(constitution)

    context = {
      'baseURL': settings.BASE_URL,
      'GATrackingId': settings.GA_TRACKING_ID,
      'speechContent': speechContent,
      'biographyContent': biographyContent,
      'booksContent': booksContent,
      'resourcesContent': resourcesContent,
      'constitutionContent': constitutionContent,
      'navBar': 'read'
    }
    return render(request, 'content/read.html', context)

# Get content of watch section
def getmedia(request):
    if translation.LANGUAGE_SESSION_KEY in request.session:
      lang_id = request.session[translation.LANGUAGE_SESSION_KEY]
    else:
      lang_id = 'en'

    livingGandhians = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 5 AND ch.lang = '{}' AND ch.active = 1 order by ch.seq asc""".format(lang_id)

    rgci = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 6 AND ch.lang = '{}' AND ch.active = 1 order by ch.seq asc""".format(lang_id)

    plays = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 7 AND ch.lang = '{}' AND ch.active = 1 order by ch.seq asc""".format(lang_id)

    resource = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 8 AND ch.lang = '{}' AND ch.active = 1 order by ch.seq asc""".format(lang_id)

    livingGandhiansContent = Headers.objects.raw(livingGandhians)
    if not livingGandhiansContent :
      livingGandhians = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 5 AND ch.lang = 'en' AND ch.active = 1 order by ch.seq asc"""
    livingGandhiansContent = Headers.objects.raw(livingGandhians)

    for content in livingGandhiansContent :
      content.youtube_id = content.iframe_url.rsplit('/', 1)[-1]

    rgciContent = Headers.objects.raw(rgci)
    if not rgciContent :
      rgci = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 6 AND ch.lang = 'en' AND ch.active = 1 order by ch.seq asc"""
    rgciContent = Headers.objects.raw(rgci)

    for content in rgciContent :
      content.youtube_id = content.iframe_url.rsplit('/', 1)[-1]

    playsContent = Headers.objects.raw(plays)
    if not playsContent :
      plays = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 7 AND ch.lang = 'en' AND ch.active = 1 order by ch.seq asc"""
    playsContent = Headers.objects.raw(plays)

    for content in playsContent :
      content.youtube_id = content.iframe_url.rsplit('/', 1)[-1]

    resourceContent = Headers.objects.raw(resource)
    if not resourceContent :
      resource = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 8 AND ch.lang = 'en' AND ch.active = 1 order by ch.seq asc"""
    resourceContent = Headers.objects.raw(resource)

    for content in resourceContent :
      content.youtube_id = content.iframe_url.rsplit('/', 1)[-1]

    x = """ SELECT id, summary FROM content_headers
        WHERE category_id = 2 AND sub_category_id = 10 AND active = 1 AND summary is not null GROUP BY sub_category_id""".format(lang_id)

    workshopheader = Headers.objects.raw(x)

    workshop = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 10 AND ch.lang = '{}' AND ch.active = 1 order by ch.seq asc""".format(lang_id)

    workshopContent = Headers.objects.raw(workshop)
    if not workshopContent :
      workshop = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 10 AND ch.lang = 'en' AND ch.active = 1 order by ch.seq asc"""
    workshopContent = Headers.objects.raw(workshop)

    az = """ SELECT id, summary FROM content_headers
        WHERE category_id = 2 AND sub_category_id = 12 AND active = 1 AND summary is not null GROUP BY sub_category_id""".format(lang_id)

    stemathomeheader = Headers.objects.raw(az)

    stemathome = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 12 AND ch.lang = '{}' AND ch.active = 1 order by ch.seq asc""".format(lang_id)

    stemathomeContent = Headers.objects.raw(stemathome)
    if not stemathomeContent :
      stemathome = """ SELECT *,cd.summary FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE ch.category_id = 2 AND ch.sub_category_id = 12 AND ch.lang = 'en' AND ch.active = 1 order by ch.seq asc"""
    stemathomeContent = Headers.objects.raw(stemathome)

    for content in stemathomeContent :
      content.youtube_id = content.iframe_url.rsplit('/', 1)[-1]

    context = {
      'baseURL': settings.BASE_URL,
      'GATrackingId': settings.GA_TRACKING_ID,
      'livingGandhiansContent': livingGandhiansContent,
      'rgciContent': rgciContent,
      'playsContent': playsContent,
      'resourceContent': resourceContent,
      'workshopheader': workshopheader,
      'stemathomeheader': stemathomeheader,
      'stemathomeContent': stemathomeContent,
      'workshopContent': workshopContent,
      'navBar': 'watch'
    }
    return render(request, 'content/watch.html', context)

# Get single speech details
def getspeech(request, header_id):
    if translation.LANGUAGE_SESSION_KEY in request.session:
      lang_id = request.session[translation.LANGUAGE_SESSION_KEY]
    else:
      lang_id = 'en'

    q = """ SELECT *,cd.summary, cd.text FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE cd.language_locale_id = {} AND cd.lang = '{}' """.format(header_id,lang_id)

    speechdetails = Headers.objects.raw(q)
    if speechdetails:
      context = {
        'baseURL': settings.BASE_URL,
        'GATrackingId': settings.GA_TRACKING_ID,
        'speechdetails': speechdetails[0],
        'navBar': 'read'
      }
    else:
      context = {
        'baseURL': settings.BASE_URL,
        'GATrackingId': settings.GA_TRACKING_ID,
        'speechdetails': speechdetails,
        'navBar': 'read'
      }

    return render(request, 'content/speech.html', context)

# Get single biography details
def getbiography(request, header_id):
    if translation.LANGUAGE_SESSION_KEY in request.session:
      lang_id = request.session[translation.LANGUAGE_SESSION_KEY]
    else:
      lang_id = 'en'

    q = """ SELECT *,cd.summary, cd.text FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        WHERE cd.language_locale_id = {} AND cd.lang = '{}' """.format(header_id, lang_id)

    biographydetails = Headers.objects.raw(q)
    if biographydetails:
      context = {
        'baseURL': settings.BASE_URL,
        'header_id': header_id,
        'GATrackingId': settings.GA_TRACKING_ID,
        'biographydetails': biographydetails[0],
        'navBar': 'read'
      }
    else:
      context = {
        'baseURL': settings.BASE_URL,
        'header_id': header_id,
        'GATrackingId': settings.GA_TRACKING_ID,
        'biographydetails': biographydetails,
        'navBar': 'read'
      }

    return render(request, 'content/biography.html', context)

# get single book details
def getbooks(request, header_id):
    if translation.LANGUAGE_SESSION_KEY in request.session:
      lang_id = request.session[translation.LANGUAGE_SESSION_KEY]
    else:
      lang_id = 'en'

    q = """ SELECT ch.*, cd.*, cb.*, cd.summary as detailSummary, cb.thumbnail_file as bookimg FROM content_headers ch
        left join content_details cd on ch.id=cd.header_id
        left join content_book cb on cd.id=cb.details_id
        WHERE ch.language_locale_id = {} AND ch.lang = '{}' """.format(header_id, lang_id)
    booksdetails = Headers.objects.raw(q)

    r = """ SELECT id, thumbnail_file,summary from content_headers WHERE language_locale_id = {} """.format(header_id)
    metaData = Headers.objects.raw(r)

    if booksdetails:
      context = {
      'baseURL': settings.BASE_URL,
      'GATrackingId': settings.GA_TRACKING_ID,
      'booksTags': booksdetails[0],
      'booksTitle': booksdetails[0].title,
      'contentType': booksdetails[0].content_type,
      'booksdetails': booksdetails,
      'metaData': metaData[0],
      'navBar': 'read'
      }
    else:
      context = {
      'baseURL': settings.BASE_URL,
      'GATrackingId': settings.GA_TRACKING_ID,
      'booksTags': booksdetails,
      'metaData': metaData[0],
      'navBar': 'read'
      }

    return render(request, 'content/books.html', context)

# Not in use
def gettest(request):
  context = {
      'navBar': 'test'
  }
  return render(request, 'content/test.html', context)

# get single video to play
def watchvideo(request, header_id):

  q = """ SELECT * FROM content_headers WHERE id = {} """.format(header_id)

  workshopVideo = Headers.objects.raw(q)

  context = {
    'baseURL': settings.BASE_URL,
    'GATrackingId': settings.GA_TRACKING_ID,
    'workshopVideo': workshopVideo[0]
  }
  return render(request, 'content/watch-video.html', context)
