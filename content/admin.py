from django.contrib import admin
from django.contrib.admin import ModelAdmin
from .models import Headers, Details, Category, SubCategory, Book, LanguageLocale
from django.utils.safestring import mark_safe
from django_summernote.admin import SummernoteModelAdmin

# Register your models here.

# Hide field if user do not have permission
def remove_from_fieldsets(fieldsets, fields):
    for fieldset in fieldsets:
        for field in fields:
            if field in fieldset[1]['fields']:
                new_fields = []
                for new_field in fieldset[1]['fields']:
                    if not new_field in fields:
                        new_fields.append(new_field)
                fieldset[1]['fields'] = tuple(new_fields)
                break

class CustomHeaderAdmin(SummernoteModelAdmin):

    list_display = ['title', 'lang', 'age_min', 'age_max', 'category',
                    'sub_category', 'content_type', 'meta_type', 'active', 'seq']
    list_filter = ('lang','category','sub_category','content_type','active')
    list_per_page = 25
    #date_hierarchy = 'pub_date'
    summernote_fields = ''

    # Hide field if user do not have permission
    def get_fieldsets(self, request, obj=None):
        fieldsets = super(CustomHeaderAdmin, self).get_fieldsets(request, obj)
        if not request.user.is_superuser and request.user.groups.filter(name='publisher').count() == 0:
            remove_from_fieldsets(fieldsets, ('active','pub_date',))
        return fieldsets


    readonly_fields = ["thumbnail_image"]
    def thumbnail_image(self, obj):
        return mark_safe('<img src="{url}" width="300" height="auto" />'.format(
            url = obj.thumbnail_file.url,
            )
    )
    class Media:
        js = (
            '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', # jquery
            'assets/js/headers-admin.js',
            )


class PhotoInline(admin.TabularInline):
    model = Book
    # extra = 1

    def get_extra(self, request, obj=None, **kwargs):
        if obj is not None and obj.book_set.count() :
            return 0
        else:
            return 1

class CustomDetailsAdmin(SummernoteModelAdmin):

    inlines = [PhotoInline]

    list_display = ['title', 'header', Details.get_header_category,
                    Details.get_header_sub_category, 'lang']
    list_filter = ('lang',)

    Details.get_header_category.short_description = 'Category'
    Details.get_header_sub_category.short_description = 'Sub-Category'
    list_per_page = 25
    # summernote_fields = '__all__'
    summernote_fields = ('text',)
    class Media:
        js = (
            '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', # jquery
            'assets/js/header-details-admin.js',
            )


class CategoryAdmin(ModelAdmin):
    list_display = ['title', 'active', 'seq'] 
    list_filter = ('title','active')

    # Hide field if user do not have permission
    def get_fieldsets(self, request, obj=None):
        fieldsets = super(CategoryAdmin, self).get_fieldsets(request, obj)
        if not request.user.is_superuser and request.user.groups.filter(name='publisher').count() == 0:
            remove_from_fieldsets(fieldsets, ('active','pub_date',))
        return fieldsets

    def has_delete_permission(self, request, obj=None):
        return False

class SubCategoryAdmin(ModelAdmin):
    list_display = ['title', 'active', SubCategory.get_header_category, 'seq'] 
    list_filter = ('title','active')
    SubCategory.get_header_category.short_description = 'Sub Category'

    # Hide field if user do not have permission
    def get_fieldsets(self, request, obj=None):
        fieldsets = super(SubCategoryAdmin, self).get_fieldsets(request, obj)
        if not request.user.is_superuser and request.user.groups.filter(name='publisher').count() == 0:
            remove_from_fieldsets(fieldsets, ('active','pub_date',))
        return fieldsets

    def has_delete_permission(self, request, obj=None):
        return False

class LanguageLocaleAdmin(ModelAdmin):
    list_display = ['lang_key', 'lang', 'translation'] 

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(Headers, CustomHeaderAdmin)
admin.site.register(Details, CustomDetailsAdmin)
admin.site.register(Category,CategoryAdmin)
admin.site.register(SubCategory,SubCategoryAdmin)
admin.site.register(LanguageLocale,LanguageLocaleAdmin)
