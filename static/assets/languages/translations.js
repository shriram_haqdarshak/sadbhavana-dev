﻿var dict = {
  "home": {
    hn: "होम पेज",
    en: "Home",
    pn: "ਮੁੱਖ ਪੰਨਾ"
  },
  "profile": {
    hn: "प्रोफाइल",
    en: "Profile",
    pn: "ਪ੍ਰੋਫਾਈਲ"
  },
  "login": {
    hn: "साइन इन करें",
    en: "Login",
    pn: "ਸਾਈਨ ਇਨ ਕਰੋ"
  },
  "account_settings": {
    hn: "अकाउंट सेटिंग",
    en: "Account Settings",
    pn: "ਖਾਤਾ ਯੋਜਨਾ"
  },
  "sign_out": {
    hn: "साइन आउट",
    en: "Sign Out",
    pn: "ਸਾਈਨ ਆਉਟ ਕਰੋ"
  },
  "register_now": {
    hn: "अभी पंजीकरण करें",
    en: "Register Now",
    pn: "ਹੁਣ ਰਜਿਸਟਰ ਕਰੋ"
  },
  "my_quizzes": {
    hn: "मेरी प्रश्नोत्तरी",
    en: "My Quizzes",
    pn: "ਮੇਰੀ ਕਵਿਜ਼"
  },
  "samvidhan": {
    hn: "संविधान",
    en: "samvidhan",
    pn: "ਸੰਵਿਧਾਨ"
  },
  "sadbhavana": {
    hn: "सद्भावना",
    en: "sadbhavana",
    pn: "ਸਦਭਾਵਨਾ"
  },
  "samadhan": {
    hn: "समाधान",
    en: "samadhaan",
    pn: "ਸਮਾਧਾਨ"
  },
  "read": {
    hn: "पढ़ें",
    en: "Read",
    pn: "ਪੜ੍ਹੋ"
  },
  "watch": {
    hn: "देखें",
    en: "Watch",
    pn: "ਦੇਖੋ"
  },
  "challenge": {
    hn: "चुनौती",
    en: "Challenge",
    pn: "ਚੁਣੌਤੀ"
  },
  "engage": {
    hn: "संलग्न",
    en: "Engage",
    pn: "ਰੁੱਝੇ ਹੋਏ"
  },
  "footer_text": {
    hn: "इंटेगर पॉज़ुअर एर्ट एनेट वेनेनटिस डेपिबस पॉसुअर वेलिट एलिकेट",
    en: "Integer posuere erat a ante venenatis dapibus posuere velit aliquet.",
    pn: "ਪੂਰਨ ਅੰਕ ਦਾ ਵੇਰਵਾ ਇਸਤੇਮਾਲ ਨਹੀਂ ਕੀਤਾ ਜਾ ਸਕਦਾ"
  },
  "latest_quizzes": {
    hn: "नवीनतम चुनौती",
    en: "LATEST QUIZZES",
    pn: "ਆਖਰੀ ਕੁਇਜ਼"
  },
  "quiz_on_mg": {
    hn: "महात्मा गाँधी पर प्रश्नोत्तरी",
    en: "QUIZ ON MAHATMA GANDHI",
    pn: "ਮਹਾਤਮਾ ਗਾਂਧੀ ਤੇ ਕੁਇਜ਼"
  },
  "quiz_on_rg": {
    hn: "राजीव गाँधी पर प्रश्नोत्तरी",
    en: "QUIZ ON RAJIV GANDHI",
    pn: "ਰਾਜੀਵ ਗਾਂਧੀ 'ਤੇ ਕੁਇਜ਼"
  },
  "quiz_on_samvidhan": {
    hn: "संविधान पर प्रश्नोत्तरी",
    en: "QUIZ ON SAMVIDHAN",
    pn: "ਸੰਵਿਧਾਨ 'ਤੇ ਕੁਇਜ਼"
  },
  "explore": {
    hn: "अन्वेषण",
    en: "EXPLORE",
    pn: "ਪੜਚੋਲ"
  },
  "keep_in_touch": {
    hn: "हमसे संपर्क करें",
    en: "CONTACT US",
    pn: "ਸੰਪਰਕ ਕਰੋ"
  },
  "keep_in_touch_newsleter": {
    hn: "महत्वपूर्ण समाचार और प्रस्ताव प्राप्त करने के लिए हमारे न्यूज़लेटर की सदस्यता लें",
    en: "KEEP IN TOUCH",
    pn: "ਮਹੱਤਵਪੂਰਣ ਖ਼ਬਰਾਂ ਅਤੇ ਪੇਸ਼ਕਸ਼ਾਂ ਪ੍ਰਾਪਤ ਕਰਨ ਲਈ ਸਾਡੇ ਨਿletਜ਼ਲੈਟਰ ਲਈ ਮੈਂਬਰ ਬਣੋ"
  },
  "explore_sadbhavana": {
    hn: "सदभावना का अन्वेषण करें",
    en: "Explore Sadbhavana",
    pn: "ਸਦਭਾਵਨਾ ਦੀ ਪੜਚੋਲ ਕਰੋ"
  },
  "terms_and_conditions": {
    hn: "नियम एवं शर्तें",
    en: "Terms and Conditions",
    pn: "ਨਿਯਮ ਤੇ ਸ਼ਰਤਾਂ"
  },
  "privacy": {
    hn: "गोपनीयता नीति",
    en: "Privacy Policy",
    pn: "ਪਰਾਈਵੇਟ ਨੀਤੀ"
  },
  "all_rights_reserved": {
    hn: "सभी अधिकार सुरक्षित, राजीव गांधी फाउंडेशन, नई दिल्ली| पंजाब सरकार द्वारा समर्थित",
    en: "All Rights Reserved, Rajiv Gandhi Foundation, New Delhi. Supported by Punjab Government",
    pn: "ਸਾਰੇ ਹੱਕ ਰਾਖਵੇਂ ਹਨ, ਰਾਜੀਵ ਗਾਂਧੀ ਫਾਉਂਡੇਸ਼ਨ, ਨਵੀਂ ਦਿੱਲੀ. ਪੰਜਾਬ ਸਰਕਾਰ ਵੱਲੋਂ ਸਮਰਥਨ ਕੀਤਾ ਗਿਆ"
  },
  "supported_by": {
    hn: "पंजाब सरकार द्वारा समर्थित",
    en: "Supported by Punjab Government",
    pn: "ਪੰਜਾਬ ਸਰਕਾਰ ਵੱਲੋਂ ਸਮਰਥਨ ਕੀਤਾ ਗਿਆ"
  },
  "speech": {
    hn: "भाषण",
    en: "Speeches",
    pn: "ਭਾਸ਼ਣ"
  },
  "biographies": {
    hn: "जीवनी",
    en: "Biographies",
    pn: "ਜੀਵਨੀ"
  },
  "resource": {
    hn: "संसाधन",
    en: "Resources",
    pn: "ਸਰੋਤ"
  },
  "books": {
    hn: "किताबें",
    en: "Books",
    pn: "ਕਿਤਾਬਾਂ"
  },
  "read_more": {
    hn: "अधिक पढ़ें",
    en: "Read More",
    pn: "ਹੋਰ ਪੜ੍ਹੋ"
  },
  "watch_this_space": {
    hn: "अधिक सामग्री के लिए यह स्थान देखें",
    en: "Watch this space for more content.",
    pn: "ਵਧੇਰੇ ਸਮੱਗਰੀ ਲਈ ਇਹ ਥਾਂ ਵੇਖੋੋ"
  },
  "about_us": {
    hn: "हमारे बारे में",
    en: "About Us",
    pn: "ਸਾਡੇ ਬਾਰੇ"
  },
  "category": {
    hn: "वर्ग",
    en: "Category",
    pn: "ਸ਼੍ਰੇਣੀ"
  },
  "registered_user": {
    hn: "मैं एक पंजीकृत उपयोगकर्ता हूँ",
    en: "I'm a registered user",
    pn: "ਮੈਂ ਇੱਕ ਰਜਿਸਟਰਡ ਉਪਭੋਗਤਾ ਹਾਂ"
  },
  "email_mobile": {
    hn: "ईमेल / मोबाइल",
    en: "Email/Mobile",
    pn: "ਈਮੇਲ / ਮੋਬਾਈਲ"
  },
  "password": {
    hn: "पासवर्ड (न्यूनतम 6 वर्ण)",
    en: "Password (Mininum 6 Characters)",
    pn: "ਪਾਸਵਰਡ (ਘੱਟੋ ਘੱਟ 6 ਅੱਖਰ)"
  },
  "register": {
    hn: "रजिस्टर करें",
    en: "Register",
    pn: "ਰਜਿਸਟਰ"
  },
  "dont_have_an_account": {
    hn: "अब तक कोई खाता नहीं है?",
    en: "Don't have an account yet?",
    pn: "ਕੀ ਤੁਹਾਡੇ ਕੋਲ ਅਜੇ ਕੋਈ ਖਾਤਾ ਨਹੀਂ ਹੈ?"
  },
  "first_name": {
    hn: "पहला नाम",
    en: "First Name",
    pn: "ਪਹਿਲਾ ਨਾਂ"
  },
  "last_name": {
    hn: "उपनाम",
    en: "Last Name",
    pn: "ਆਖਰੀ ਨਾਂਮ"
  },
  "date_of_birth": {
    hn: "जन्म तिथि (कृपया DD-MM-YYYY प्रारूप में दर्ज करें)",
    en: "Date of Birth (Please enter in DD-MM-YYYY format)",
    pn: "ਜਨਮ ਮਿਤੀ (ਕਿਰਪਾ ਕਰਕੇ DD-MM-YYYY ਫਾਰਮੈਟ ਵਿੱਚ ਦਾਖਲ ਕਰੋ)"
  },
  "gender": {
    hn: "लिंग",
    en: "Gender",
    pn: "ਲਿੰਗ"
  },
  "select": {
    hn: "--- चुने ---",
    en: "--- Select ---",
    pn: "--- ਚੁਣੋ ---"
  },
  "male": {
    hn: "पुरुष",
    en: "Male",
    pn: "ਨਰ"
  },
  "female": {
    hn: "महिला",
    en: "Female",
    pn: ".ਰਤ"
  },
  "other": {
    hn: "अन्य",
    en: "Other",
    pn: "ਹੋਰ"
  },
  "email": {
    hn: "ईमेल",
    en: "Email",
    pn: "ਈ - ਮੇਲ"
  },
  "mobile": {
    hn: "मोबाइल",
    en: "Mobile",
    pn: "ਮੋਬਾਈਲ"
  },
  "confirm_password": {
    hn: "पासवर्ड की पुष्टि कीजिये",
    en: "Confirm Password",
    pn: "ਪਾਸਵਰਡ ਪੱਕਾ ਕਰੋ"
  },
  "state": {
    hn: "राज्य",
    en: "State",
    pn: "ਰਾਜ"
  },
  "district": {
    hn: "जिला",
    en: "District",
    pn: "ਜ਼ਿਲ੍ਹਾ"
  },

  // "punjab": {
  //   hn: "पंजाब",
  //   en: "Punjab",
  //   pn: "ਪੰਜਾਬ"
  // },

  "amritsar": { hn: "अमृतसर", en: "Amritsar", pn: "ਅੰਮ੍ਰਿਤਸਰ" },
  "barnala": { hn: "बरनाला", en: "Barnala", pn: "ਬਰਨਾਲਾ" },
  "bathinda": { hn: "बठिंडा", en: "Bathinda", pn: "ਬਠਿੰਡਾ" },
  "faridkot": { hn: "फरीदकोट", en: "Faridkot", pn: "ਫਰੀਦਕੋਟ" },
  "fatehgarh_sahib": { hn: "फतेहगढ़ साहिब", en: "Fatehgarh Sahib", pn: "ਫਤਿਹਗੜ੍ਹ ਸਾਹਿਬ" },
  "fazilka": { hn: "फाजिल्का", en: "Fazilka", pn: "ਫਾਜ਼ਿਲਕਾ" },
  "ferozepur": { hn: "फिरोजपुर", en: "Ferozepur", pn: "ਫਿਰੋਜ਼ਪੁਰ" },
  "gurdaspur": { hn: "गुरदासपुर", en: "Gurdaspur", pn: "ਗੁਰਦਾਸਪੁਰ" },
  "hoshiarpur": { hn: "होशियारपुर", en: "Hoshiarpur", pn: "ਹੁਸ਼ਿਆਰਪੁਰ" },
  "jalandhar": { hn: "जालंधर", en: "Jalandhar", pn: "ਜਲੰਧਰ" },
  "kapurthala": { hn: "कपूरथला", en: "Kapurthala", pn: "ਕਪੂਰਥਲਾ" },
  "ludhiana": { hn: "लुधियाना", en: "Ludhiana", pn: "ਲੁਧਿਆਣਾ" },
  "mansa": { hn: "मनसा", en: "Mansa", pn: "ਮਾਨਸਾ" },
  "moga": { hn: "मोगा", en: "Moga", pn: "ਮੋਗਾ" },
  "pathankot": { hn: "पठानकोट", en: "Pathankot", pn: "ਪਠਾਨਕੋਟ" },
  "patiala": { hn: "पटियाला", en: "Patiala", pn: "ਪਟਿਆਲਾ" },
  "roop_nagar": { hn: "रूप नगर", en: "Roop Nagar", pn: "ਰੂਪਨਗਰ" },
  "sas_nagar": { hn: "एस.ए.एस नगर", en: "S.A.S Nagar", pn: "ਐਸ.ਏ.ਐਸ ਨਗਰ" },
  "sbs_nagar": { hn: "एस.बी.एस नगर", en: "S.B.S Nagar", pn: "ਐਸ.ਬੀ.ਐਸ ਨਗਰ" },
  "sangrur": { hn: "संगरुर", en: "Sangrur", pn: "ਸੰਗਰੂਰ" },
  "sri_mukatsar_sahib": { hn: "श्री मुक्तसर साहिब", en: "Sri Mukatsar Sahib", pn: "ਸ਼੍ਰੀ ਮੁਕਤਸਰ ਸਾਹਿਬ" },
  "tarn_taran": { hn: "टार्न-तरन", en: "Tarn-Taran", pn: "ਤਰਨ ਤਾਰਨ" },



  "terms_of_service": {
    hn: "सेवा की शर्तें",
    en: "Terms Of Service",
    pn: "ਸੇਵਾ ਦੀਆਂ ਸ਼ਰਤਾਂ"
  },
  "agree_to_the": {
    hn: "मैं इसके लिए सहमत हूँ",
    en: "I agree to the",
    pn: "ਮੈਂ ਸਹਿਮਤ ਹਾਂ"
  },

  "save": {
    hn: "सेव करें",
    en: "Save",
    pn: "ਸੇਵ ਕਰੋ"
  },

  "stories": {
    hn: "कहानी",
    en: "Stories",
    pn: "ਕਹਾਣੀਆਂ"
  },
  "media": {
    hn: "मीडिया",
    en: "Media",
    pn: "ਮੀਡੀਆ"
  },

  "returning_user": {
    hn: "मैं एक लौटने वाला उपयोगकर्ता हूं",
    en: "I'm a returning user",
    pn: "ਮੈਂ ਇੱਕ ਵਾਪਸੀ ਕਰਨ ਵਾਲਾ ਉਪਭੋਗਤਾ ਹਾਂ"
  },
  "dont_have_account": {
    hn: "अभी तक खाता नहीं है?",
    en: "Don't have an account yet?",
    pn: "ਕੀ ਤੁਹਾਡੇ ਕੋਲ ਅਜੇ ਕੋਈ ਖਾਤਾ ਨਹੀਂ ਹੈ?"
  },

  "living_gandhians": {
    hn: "जीवित गांधीवादी",
    en: "Living Gandhians",
    pn: "ਜੀਵਤ ਗਾਂਧੀਵਾਦੀ"
  },
  "rajiv_gandhi_contribution_to_india": {
    hn: "राजीव गांधी: भारत में योगदान",
    en: "Rajiv Gandhi: Contribution to India",
    pn: "ਰਾਜੀਵ ਗਾਂਧੀ: ਭਾਰਤ ਲਈ ਯੋਗਦਾਨ"
  },
  "plays": {
    hn: "नाटक",
    en: "Plays",
    pn: "ਖੇਡੋ"
  },
  "my_points": {
    hn: "मेरे अंक",
    en: "MY POINTS",
    pn: "ਮੇਰੇ ਅੰਕ"
  },
  "taken_on": {
    hn: "पर लिया गया",
    en: "Taken On",
    pn: "'ਤੇ ਲਿਆ"
  },
  "score": {
    hn: "स्कोर",
    en: "Score",
    pn: "ਸਕੋਰ"
  },
  "my_profile": {
    hn: "मेरी प्रोफाइल",
    en: "My Profile",
    pn: "ਮੇਰਾ ਪ੍ਰੋਫਾਈਲ"
  },
  "quizzes": {
    hn: "प्रश्नोत्तरी",
    en: "Quizzes",
    pn: "ਕੁਇਜ਼"
  },
  "play": {
    hn: "खेलें",
    en: "Play",
    pn: "ਖੇਡੋ"
  },
  "quiz": {
    hn: "प्रश्नोत्तरी",
    en: "Quiz",
    pn: "ਕੁਇਜ਼"
  },
  "now": {
    hn: "अब",
    en: "Now",
    pn: "ਹੁਣ"
  },
  "more": {
    hn: "और",
    en: "More",
    pn: "ਹੋਰ"
  },
  "search_results": {
    hn: "खोज परिणाम",
    en: "Search Results",
    pn: "ਖੋਜ ਨਤੀਜੇ"
  },
  "categories": {
    hn: "श्रेणियाँ",
    en: "Categories",
    pn: "ਵਰਗ"
  },
  "tryst_with_destiny_neharu_speech": {
    hn: "भाग्य के साथ प्रयास करें नेहरू के साथ",
    en: "Tryst with Destiny Nehru Speech",
    pn: "ਕਿਸਮਤ ਨਹਿਰੂ ਸਪੀਚ ਨਾਲ"
  },

  "privacy_policy_text_p1": {
    hn: "राजीव गांधी फाउंडेशन ने व्यक्तिगत जानकारी के संग्रह और प्रसार के बारे में अपनी प्रथाओं को संप्रेषित करने के लिए इस गोपनीयता नीति का निर्माण किया है, जिसे किसी विशिष्ट व्यक्ति या किसी अन्य जानकारी से जोड़ा जा सकता है, जो वेबसाइट आगंतुकों को प्रदान की जाती है। फाउंडेशन किसी भी व्यक्तिगत जानकारी का उपयोग केवल उसी उद्देश्य के लिए करेगा जिसके लिए वह प्रस्तुत किया गया है और हम ऐसी सूचनाओं का उपयोग परिचालन नोटिस प्रदान करने के लिए करेंगे जो हमारे कार्यक्रम के उद्देश्यों और मिशन को पूरा करने के लिए आवश्यक माना जाता है।",
    en: "The Rajiv Gandhi Foundation has created this privacy policy to communicate its practices regarding the collection and dissemination of personal information that can be linked to a specific individual or any other information, provided to website visitors. The Foundation will use any personal information only for the purpose for which it is submitted and we will use such information to provide operational notices as deemed necessary to fulfill our program purposes and mission.",
    pn: "ਰਾਜੀਵ ਗਾਂਧੀ ਫਾਉਂਡੇਸ਼ਨ ਨੇ ਇਹ ਗੁਪਤ ਨੀਤੀ ਤਿਆਰ ਕੀਤੀ ਹੈ ਜੋ ਆਪਣੀ ਨਿੱਜੀ ਜਾਣਕਾਰੀ ਨੂੰ ਇਕੱਤਰ ਕਰਨ ਅਤੇ ਇਸ ਦੇ ਪ੍ਰਸਾਰ ਸੰਬੰਧੀ ਆਪਣੇ ਅਭਿਆਸਾਂ ਨੂੰ ਸੰਚਾਰਿਤ ਕਰਨ ਲਈ ਬਣਾਈ ਹੈ ਜੋ ਕਿਸੇ ਖਾਸ ਵਿਅਕਤੀ ਜਾਂ ਕਿਸੇ ਹੋਰ ਜਾਣਕਾਰੀ ਨਾਲ ਜੁੜ ਸਕਦੀ ਹੈ, ਜੋ ਵੈਬਸਾਈਟ ਵਿਜ਼ਟਰਾਂ ਨੂੰ ਪ੍ਰਦਾਨ ਕੀਤੀ ਜਾਂਦੀ ਹੈ. ਫਾਉਂਡੇਸ਼ਨ ਕੋਈ ਵੀ ਨਿੱਜੀ ਜਾਣਕਾਰੀ ਸਿਰਫ ਇਸ ਉਦੇਸ਼ ਲਈ ਵਰਤੇਗੀ ਜਿਸਦੇ ਲਈ ਇਹ ਜਮ੍ਹਾ ਕੀਤੀ ਗਈ ਹੈ ਅਤੇ ਅਸੀਂ ਅਜਿਹੀ ਜਾਣਕਾਰੀ ਦੀ ਵਰਤੋਂ ਕਾਰਜ ਪ੍ਰਣਾਲੀ ਦੇ ਨੋਟਿਸ ਪ੍ਰਦਾਨ ਕਰਨ ਲਈ ਕਰਾਂਗੇ ਜੋ ਸਾਡੇ ਪ੍ਰੋਗਰਾਮ ਦੇ ਉਦੇਸ਼ਾਂ ਅਤੇ ਮਿਸ਼ਨ ਨੂੰ ਪੂਰਾ ਕਰਨ ਲਈ ਜ਼ਰੂਰੀ ਸਮਝੀ ਜਾਂਦੀ ਹੈ."
  },
  "privacy_policy_text_p2": {
    hn: "फाउंडेशन कभी भी किसी तीसरे पक्ष के साथ किसी भी उपयोगकर्ता की व्यक्तिगत जानकारी को बेचता या बेचता नहीं है, जहां उसे भूमि के कानून की आवश्यकता होती है। फाउंडेशन की वेब साइट आगंतुकों द्वारा दर्ज नहीं की गई व्यक्तिगत जानकारी को ट्रैक करने, एकत्र करने या वितरित करने के लिए सेट नहीं है। अनधिकृत पहुंच को रोकने के लिए, डेटा सटीकता बनाए रखें, और सूचना के सही उपयोग को सुनिश्चित करें, फाउंडेशन ने ऑनलाइन एकत्र की गई जानकारी को सुरक्षित रखने और सुरक्षित करने के लिए उचित भौतिक, इलेक्ट्रॉनिक और प्रबंधकीय प्रक्रियाओं को रखा है। क्या किसी भी उपयोगकर्ता को इन गोपनीयता नीतियों के बारे में अन्य प्रश्न या चिंताएं हैं, कृपया हमसे संपर्क करें।",
    en: "The Foundation never sells or in any other way shares any user’s personal information with any third parties except where it is required by law of the land. The Foundation’s web site is not set up to track, collect or distribute personal information not entered by visitors. To prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, the Foundation has put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the information collected online. Should any user have other questions or concerns about these privacy policies, please contact us.",
    pn: "ਫਾਉਂਡੇਸ਼ਨ ਕਦੇ ਵੀ ਕਿਸੇ ਵੀ ਉਪਭੋਗਤਾ ਦੀ ਨਿੱਜੀ ਜਾਣਕਾਰੀ ਨੂੰ ਕਿਸੇ ਤੀਜੀ ਧਿਰ ਨਾਲ ਨਹੀਂ ਵੇਚਦਾ ਅਤੇ ਨਾ ਹੀ ਕਿਸੇ ਹੋਰ ਤਰੀਕੇ ਨਾਲ ਸਾਂਝਾ ਕਰਦਾ ਹੈ ਸਿਵਾਏ ਉਸ ਜਗ੍ਹਾ ਨੂੰ ਜਿੱਥੇ ਜ਼ਮੀਨ ਦੇ ਕਾਨੂੰਨ ਦੁਆਰਾ ਲੋੜੀਂਦਾ ਹੁੰਦਾ ਹੈ. ਫਾਉਂਡੇਸ਼ਨ ਦੀ ਵੈੱਬ ਸਾਈਟ ਸੈਲਾਨੀ ਨਹੀਂ ਕੀਤੀ ਗਈ ਹੈ ਜੋ ਸੈਲਾਨੀਆਂ ਦੁਆਰਾ ਦਾਖਲ ਨਹੀਂ ਕੀਤੀ ਗਈ ਨਿੱਜੀ ਜਾਣਕਾਰੀ ਨੂੰ ਟਰੈਕ ਕਰਨ, ਇਕੱਤਰ ਕਰਨ ਜਾਂ ਵੰਡਣ ਲਈ ਕੀਤੀ ਗਈ ਹੈ. ਅਣਅਧਿਕਾਰਤ ਪਹੁੰਚ ਨੂੰ ਰੋਕਣ ਲਈ, ਅੰਕੜਿਆਂ ਦੀ ਸ਼ੁੱਧਤਾ ਨੂੰ ਬਣਾਈ ਰੱਖਣ ਅਤੇ ਜਾਣਕਾਰੀ ਦੀ ਸਹੀ ਵਰਤੋਂ ਨੂੰ ਯਕੀਨੀ ਬਣਾਉਣ ਲਈ, ਫਾਉਂਡੇਸ਼ਨ ਨੇ collectedਨਲਾਈਨ ਇਕੱਠੀ ਕੀਤੀ ਜਾਣਕਾਰੀ ਨੂੰ ਸੁਰੱਖਿਅਤ ਅਤੇ ਸੁਰੱਖਿਅਤ ਕਰਨ ਲਈ physicalੁਕਵੀਂ ਭੌਤਿਕ, ਇਲੈਕਟ੍ਰਾਨਿਕ ਅਤੇ ਪ੍ਰਬੰਧਕੀ ਪ੍ਰਕਿਰਿਆਵਾਂ ਰੱਖੀਆਂ ਹਨ. ਜੇ ਕਿਸੇ ਵੀ ਉਪਭੋਗਤਾ ਨੂੰ ਇਹਨਾਂ ਗੁਪਤ ਨੀਤੀਆਂ ਬਾਰੇ ਹੋਰ ਪ੍ਰਸ਼ਨ ਜਾਂ ਚਿੰਤਾਵਾਂ ਹੋਣ, ਤਾਂ ਕਿਰਪਾ ਕਰਕੇ ਸਾਡੇ ਨਾਲ ਸੰਪਰਕ ਕਰੋ."
  },

  "close": {
    hn: "बंद करे",
    en: "Close",
    pn: "ਬੰਦ ਕਰੋ"
  },

  "andhra_pradesh": { hn: "आंध्र प्रदेश", en: "Andhra Pradesh", pn: "ਆਂਧਰਾ ਪ੍ਰਦੇਸ਼" },
  "arunachal_pradesh": { hn: "अरुणाचल प्रदेश", en: "Arunachal Pradesh", pn: "ਅਰੁਣਾਚਲ ਪ੍ਰਦੇਸ਼" },
  "assam": { hn: "असम", en: "Assam", pn: "ਅਸਾਮ" },
  "bihar": { hn: "बिहार", en: "Bihar", pn: "ਬਿਹਾਰ" },
  "chhattisgarh": { hn: "छत्तीसगढ़", en: "Chhattisgarh", pn: "ਛੱਤੀਸਗੜ" },
  "goa": { hn: "गोवा", en: "Goa", pn: "ਗੋਆ" },
  "gujarat": { hn: "गुजरात", en: "Gujarat", pn: "ਗੁਜਰਾਤ" },
  "haryana": { hn: "हरियाणा", en: "Haryana", pn: "ਹਰਿਆਣੇ" },
  "himachal_pradesh": { hn: "हिमाचल प्रदेश", en: "Himachal Pradesh", pn: "ਹਿਮਾਚਲ ਪ੍ਰਦੇਸ਼" },
  "jammu_and_kashmir": { hn: "जम्मू और कश्मीर", en: "Jammu and Kashmir", pn: "ਜੰਮੂ ਕਸ਼ਮੀਰ" },
  "jharkhand": { hn: "झारखंड", en: "Jharkhand", pn: "ਝਾਰਖੰਡ" },
  "karnataka": { hn: "कर्नाटक", en: "Karnataka", pn: "ਕਰਨਾਟਕ" },
  "kerala": { hn: "केरल", en: "Kerala", pn: "ਕੇਰਲ" },
  "madhya_pradesh": { hn: "मध्य प्रदेश", en: "Madhya Pradesh", pn: "ਮੱਧ ਪ੍ਰਦੇਸ਼" },
  "maharashtra": { hn: "महाराष्ट्र", en: "Maharashtra", pn: "ਮਹਾਰਾਸ਼ਟਰ" },
  "manipur": { hn: "मणिपुर", en: "Manipur", pn: "ਮਨੀਪੁਰ" },
  "meghalaya": { hn: "मेघालय", en: "Meghalaya", pn: "ਮੇਘਾਲਿਆ" },
  "mizoram": { hn: "मिजोरम", en: "Mizoram", pn: "ਮਿਜ਼ੋਰਮ" },
  "nagaland": { hn: "नगालैंड", en: "Nagaland", pn: "ਨਾਗਾਲੈਂਡ" },
  "odisha": { hn: "ओडिशा", en: "Odisha", pn: "ਓਡੀਸ਼ਾ" },
  "punjab": { hn: "पंजाब", en: "Punjab", pn: "ਪੰਜਾਬ" },
  "rajasthan": { hn: "राजस्थान", en: "Rajasthan", pn: "ਰਾਜਸਥਾਨ" },
  "sikkim": { hn: "सिक्किम", en: "Sikkim", pn: "ਸਿੱਕਮ" },
  "tamil_nadu": { hn: "तमिलनाडु", en: "Tamil Nadu", pn: "ਤਾਮਿਲਨਾਡੂ" },
  "telangana": { hn: "तेलंगाना", en: "Telangana", pn: "ਤੇਲੰਗਾਨਾ" },
  "tripura": { hn: "त्रिपुरा", en: "Tripura", pn: "ਤ੍ਰਿਪੁਰਾ" },
  "uttar_pradesh": { hn: "उत्तर प्रदेश", en: "Uttar Pradesh", pn: "ਉੱਤਰ ਪ੍ਰਦੇਸ਼" },
  "uttarakhand": { hn: "उत्तराखंड", en: "Uttarakhand", pn: "ਉਤਰਾਖੰਡ" },
  "west_bengal": { hn: "पश्चिम बंगाल", en: "West Bengal", pn: "ਪੱਛਮੀ ਬੰਗਾਲ" },
  "andaman_and_nicobar_islands": { hn: "अंडमान व नोकोबार द्वीप समूह", en: "Andaman and Nicobar Islands", pn: "ਅੰਡੇਮਾਨ ਅਤੇ ਨਿਕੋਬਾਰ ਟਾਪੂ" },
  "chandigarh": { hn: "चंडीगढ़", en: "Chandigarh", pn: "ਚੰਡੀਗੜ੍ਹ" },
  "dadra_and_nagar_haveli": { hn: "दादरा और नगर हवेली", en: "Dadra and Nagar Haveli", pn: "ਦਾਦਰਾ ਅਤੇ ਨਗਰ ਹਵੇਲੀ" },
  "daman_and_diu": { hn: "दमन और दीव", en: "Daman and Diu", pn: "ਦਮਨ ਅਤੇ ਦਿਉ" },
  "delhi": { hn: "दिल्ली", en: "Delhi", pn: "ਦਿੱਲੀ" },
  "ladakh": { hn: "लद्दाख", en: "Ladakh", pn: "ਲੱਦਾਖ" },
  "lakshadweep": { hn: "लक्षद्वीप", en: "Lakshadweep", pn: "ਲਕਸ਼ਦਵੀਪ" },
  "puducherry": { hn: "पुडुचेरी", en: "Puducherry", pn: "ਪੁਡੂਚੇਰੀ" },

  "proceed": {
    hn: "आगे बढ़ें",
    en: "Proceed",
    pn: "ਨਾੱਗੇ ਵਧੋ"
  },
  "go_back": {
    hn: "वापस जाएँ",
    en: "Go Back",
    pn: "ਵਾਪਸ ਜਾਓ"
  },

  "quiz_result": { hn: "प्रश्नोत्तरी परिणाम", en: "Quiz Results", pn: "ਕੁਇਜ਼ ਨਤੀਜਾ" }, "thank_you_participating_quiz": { hn: "प्रश्नोत्तरी में भाग लेने के लिए धन्यवाद", en: "Thank you for participating in the Quiz", pn: "ਕੁਇਜ਼ ਵਿਚ ਭਾਗ ਲੈਣ ਲਈ ਤੁਹਾਡਾ ਧੰਨਵਾਦ" },
  "score": { hn: "स्कोर", en: "Score", pn: "ਸਕੋਰ" },
  "forgot_password": { hn: "पासवर्ड भूल गए", en: "Forgot Password", pn: "ਪਾਸਵਰਡ ਭੁੱਲ ਗਏ"},
  "reset_password": { hn: "पासवर्ड रीसेट", en: "Reset Password", pn: "ਪਾਸਵਰਡ ਰੀਸੈਟ ਕਰੋ"},
  "instructions_for_setting_your_password": { hn: "हमने आपको अपना पासवर्ड सेट करने के निर्देश दिए हैं। यदि वे कुछ मिनटों में नहीं पहुंचे हैं, तो अपने स्पैम फ़ोल्डर की जाँच करें!", en: "We've emailed you instructions for setting your password. If they haven't arrived in a few minutes, check your spam folder.", pn: "ਅਸੀਂ ਤੁਹਾਨੂੰ ਆਪਣਾ ਪਾਸਵਰਡ ਸੈਟ ਕਰਨ ਲਈ ਨਿਰਦੇਸ਼ਾਂ ਨੂੰ ਈਮੇਲ ਕੀਤੇ ਹਨ. ਜੇ ਉਹ ਕੁਝ ਮਿੰਟਾਂ ਵਿਚ ਨਹੀਂ ਪਹੁੰਚੇ, ਤਾਂ ਆਪਣੇ ਸਪੈਮ ਫੋਲਡਰ ਦੀ ਜਾਂਚ ਕਰੋ"},
  "password_changed": {hn: "पासवर्ड बदल दिया गया है!", en: "The password has been changed!", pn: "ਪਾਸਵਰਡ ਬਦਲਿਆ ਗਿਆ ਹੈ!"},
  "log_in_again": { hn: "फिर से लॉगिन करें?", en: "Log in again?", pn: "ਦੁਬਾਰਾ ਲੌਗ ਇਨ ਕਰੋ?"},
  "confirm_your_new_password": { hn: "कृपया अपना नया पासवर्ड दर्ज करें (और पुष्टि करें)", en: "Please enter (and confirm) your new password.", pn: "ਕਿਰਪਾ ਕਰਕੇ ਆਪਣਾ ਨਵਾਂ ਪਾਸਵਰਡ ਭਰੋ (ਅਤੇ ਪੁਸ਼ਟੀ ਕਰੋ)"},
  "confirm_password": { hn: "पासवर्ड की पुष्टि कीजिये", en: "Confirm password", pn: "ਪਾਸਵਰਡ ਪੱਕਾ ਕਰੋ"},
  "new_password": { hn: "नया पासवर्ड", en: "New password", pn: "ਨਵਾਂ ਪਾਸਵਰਡ"},
  "change_my_password": { hn: "मेरा पासवर्ड बदलो", en: "Change my password", pn: "ਮੇਰਾ ਪਾਸਵਰਡ ਬਦਲੋ"},
  "password_reset_failed": { hn: "पासवर्ड रीसेट विफल रहा", en: "Password reset failed", pn: "ਪਾਸਵਰਡ ਰੀਸੈਟ ਅਸਫਲ"},
  "password_reset_link_was_invalid": { hn: "पासवर्ड रीसेट लिंक अमान्य था, संभवतः क्योंकि इसका उपयोग पहले ही किया जा चुका है। कृपया एक नया पासवर्ड रीसेट करने का अनुरोध करें।", en: "The password reset link was invalid, possibly because it has already been used. Please request a new password reset.", pn: "ਪਾਸਵਰਡ ਰੀਸੈਟ ਲਿੰਕ ਅਵੈਧ ਸੀ, ਸੰਭਵ ਤੌਰ 'ਤੇ ਕਿਉਂਕਿ ਇਹ ਪਹਿਲਾਂ ਹੀ ਵਰਤਿਆ ਜਾ ਚੁੱਕਾ ਹੈ. ਕਿਰਪਾ ਕਰਕੇ ਇੱਕ ਨਵਾਂ ਪਾਸਵਰਡ ਰੀਸੈਟ ਕਰਨ ਦੀ ਬੇਨਤੀ ਕਰੋ"},
  "feedback": { hn: "प्रतिपुष्टि", en: "Feedback", pn: "ਸੁਝਾਅ"},

  "name": { hn: "नाम", en: "Name", pn: "Name"},
  "age": { hn: "आयु", en: "Age", pn: "Age"},
  "class": { hn: "कक्षा", en: "Class", pn: "Class"},
  "profession": { hn: "व्यवसाय", en: "Profession", pn: "Profession"},
  "address": { hn: "पता", en: "Address", pn: "Address"},
  "how_did_you_learn_about_this_platform": { hn: "आपने इस प्लेटफार्म फॉर्म के बारे में", en: "How did you learn about this platform?", pn: "How did you learn about this platform?"},
  "workshop": { hn: "वर्कशॉप", en: "Workshop", pn: "Workshop"},
  "online": { hn: "ऑनलाइन", en: "Online", pn: "Online"},
  "friends": { hn: "दोस्तों से", en: "Friends", pn: "Friends"},
  "other": { hn: "अन्य", en: "Other", pn: "Other"},
  "did_you_face_any_difficulty_while_logging_in": { hn: "क्या आपको लॉगिन करते समय किसी कठिनाई का सामना करना पड़ा?", en: "Did you face any difficulty while logging in?", pn: "Did you face any difficulty while logging in?"},
  "yes": { hn: "हाँ", en: "Yes", pn: "Yes"},
  "no": { hn: "नहीं", en: "No", pn: "No"},
  "if_yes_please_explain": { hn: "अगर हाँ, कृपया विस्तार से समझाइए..", en: "If yes please explain..", pn: "If yes please explain.."},
  "which_section_on_this_platform_did_you_like_the_most": { hn: "इस प्लेटफार्म पर आपको कौन सा सेक्शन सबसे ज्यादा पसंद आया?", en: "Which section on this platform did you like the most?", pn: "Which section on this platform did you like the most?"},
  "why": { hn: "क्यों?", en: "Why?", pn: "Why?"},
  "which_section_on_this_platform_did_you_not_like": { hn: "इस प्लेटफार्म पर आपको कौन सा सेक्शन पसंद नहीं आया?", en: "Which section on this platform did you not like?", pn: "Which section on this platform did you not like?"},
  "what_would_you_like_to_see_in_this_platform": { hn: "आप इस प्लेटफॉर्म में क्या देखना चाहेंगे?", en: "What would you like to see in this platform?", pn: "What would you like to see in this platform?"},
  "what_did_you_learn_about_sadbhavana_samvidhan_samadhaan_from_this_platform": { hn: "आपने इस प्लेटफ़ोर्म पर सद्भावना,संविधान,समाधान के बारे में क्या सीखा?", en: "What did you learn about 'Sadbhavana' 'Samvidhan' 'Samadhaan' from this platform?", pn: "What did you learn about 'Sadbhavana' 'Samvidhan' 'Samadhaan' from this platform?"},
  "would_you_come_back_to_this_platform_any_time_soon": { hn: "क्या आप वापस इस प्लेटफ़ोर्म पर आएंगे?", en: "Would you come back to this platform any time soon?", pn: "Would you come back to this platform any time soon?"},
  "would_you_share_the_sadbhavana_net_platform_with_your_friends": { hn: "क्या आप आई. डी. ई. ए. एल. प्लेटफ़ोर्म के बारे में अपने दोस्तो को बताएंगे?", en: "Would you share the sadbhavana.net platform with your friends?", pn: "Would you share the sadbhavana.net platform with your friends?"},

  "may_be": { hn: "शायद", en: "May be", pn: "May be"}


}



// ref: https://stackoverflow.com/a/9893752
function getLocaleDateString() {
  var formats = {
    "ar-SA": "dd/MM/yy",
    "bg-BG": "dd.M.yyyy",
    "ca-ES": "dd/MM/yyyy",
    "zh-TW": "yyyy/M/d",
    "cs-CZ": "d.M.yyyy",
    "da-DK": "dd-MM-yyyy",
    "de-DE": "dd.MM.yyyy",
    "el-GR": "d/M/yyyy",
    "en-US": "M/d/yyyy",
    "fi-FI": "d.M.yyyy",
    "fr-FR": "dd/MM/yyyy",
    "he-IL": "dd/MM/yyyy",
    "hu-HU": "yyyy. MM. dd.",
    "is-IS": "d.M.yyyy",
    "it-IT": "dd/MM/yyyy",
    "ja-JP": "yyyy/MM/dd",
    "ko-KR": "yyyy-MM-dd",
    "nl-NL": "d-M-yyyy",
    "nb-NO": "dd.MM.yyyy",
    "pl-PL": "yyyy-MM-dd",
    "pt-BR": "d/M/yyyy",
    "ro-RO": "dd.MM.yyyy",
    "ru-RU": "dd.MM.yyyy",
    "hr-HR": "d.M.yyyy",
    "sk-SK": "d. M. yyyy",
    "sq-AL": "yyyy-MM-dd",
    "sv-SE": "yyyy-MM-dd",
    "th-TH": "d/M/yyyy",
    "tr-TR": "dd.MM.yyyy",
    "ur-PK": "dd/MM/yyyy",
    "id-ID": "dd/MM/yyyy",
    "uk-UA": "dd.MM.yyyy",
    "be-BY": "dd.MM.yyyy",
    "sl-SI": "d.M.yyyy",
    "et-EE": "d.MM.yyyy",
    "lv-LV": "yyyy.MM.dd.",
    "lt-LT": "yyyy.MM.dd",
    "fa-IR": "MM/dd/yyyy",
    "vi-VN": "dd/MM/yyyy",
    "hy-AM": "dd.MM.yyyy",
    "az-Latn-AZ": "dd.MM.yyyy",
    "eu-ES": "yyyy/MM/dd",
    "mk-MK": "dd.MM.yyyy",
    "af-ZA": "yyyy/MM/dd",
    "ka-GE": "dd.MM.yyyy",
    "fo-FO": "dd-MM-yyyy",
    "hi-IN": "dd-MM-yyyy",
    "ms-MY": "dd/MM/yyyy",
    "kk-KZ": "dd.MM.yyyy",
    "ky-KG": "dd.MM.yy",
    "sw-KE": "M/d/yyyy",
    "uz-Latn-UZ": "dd/MM yyyy",
    "tt-RU": "dd.MM.yyyy",
    "pa-IN": "dd-MM-yy",
    "gu-IN": "dd-MM-yy",
    "ta-IN": "dd-MM-yyyy",
    "te-IN": "dd-MM-yy",
    "kn-IN": "dd-MM-yy",
    "mr-IN": "dd-MM-yyyy",
    "sa-IN": "dd-MM-yyyy",
    "mn-MN": "yy.MM.dd",
    "gl-ES": "dd/MM/yy",
    "kok-IN": "dd-MM-yyyy",
    "syr-SY": "dd/MM/yyyy",
    "dv-MV": "dd/MM/yy",
    "ar-IQ": "dd/MM/yyyy",
    "zh-CN": "yyyy/M/d",
    "de-CH": "dd.MM.yyyy",
    "en-GB": "dd/MM/yyyy",
    "es-MX": "dd/MM/yyyy",
    "fr-BE": "d/MM/yyyy",
    "it-CH": "dd.MM.yyyy",
    "nl-BE": "d/MM/yyyy",
    "nn-NO": "dd.MM.yyyy",
    "pt-PT": "dd-MM-yyyy",
    "sr-Latn-CS": "d.M.yyyy",
    "sv-FI": "d.M.yyyy",
    "az-Cyrl-AZ": "dd.MM.yyyy",
    "ms-BN": "dd/MM/yyyy",
    "uz-Cyrl-UZ": "dd.MM.yyyy",
    "ar-EG": "dd/MM/yyyy",
    "zh-HK": "d/M/yyyy",
    "de-AT": "dd.MM.yyyy",
    "en-AU": "d/MM/yyyy",
    "es-ES": "dd/MM/yyyy",
    "fr-CA": "yyyy-MM-dd",
    "sr-Cyrl-CS": "d.M.yyyy",
    "ar-LY": "dd/MM/yyyy",
    "zh-SG": "d/M/yyyy",
    "de-LU": "dd.MM.yyyy",
    "en-CA": "dd/MM/yyyy",
    "es-GT": "dd/MM/yyyy",
    "fr-CH": "dd.MM.yyyy",
    "ar-DZ": "dd-MM-yyyy",
    "zh-MO": "d/M/yyyy",
    "de-LI": "dd.MM.yyyy",
    "en-NZ": "d/MM/yyyy",
    "es-CR": "dd/MM/yyyy",
    "fr-LU": "dd/MM/yyyy",
    "ar-MA": "dd-MM-yyyy",
    "en-IE": "dd/MM/yyyy",
    "es-PA": "MM/dd/yyyy",
    "fr-MC": "dd/MM/yyyy",
    "ar-TN": "dd-MM-yyyy",
    "en-ZA": "yyyy/MM/dd",
    "es-DO": "dd/MM/yyyy",
    "ar-OM": "dd/MM/yyyy",
    "en-JM": "dd/MM/yyyy",
    "es-VE": "dd/MM/yyyy",
    "ar-YE": "dd/MM/yyyy",
    "en-029": "MM/dd/yyyy",
    "es-CO": "dd/MM/yyyy",
    "ar-SY": "dd/MM/yyyy",
    "en-BZ": "dd/MM/yyyy",
    "es-PE": "dd/MM/yyyy",
    "ar-JO": "dd/MM/yyyy",
    "en-TT": "dd/MM/yyyy",
    "es-AR": "dd/MM/yyyy",
    "ar-LB": "dd/MM/yyyy",
    "en-ZW": "M/d/yyyy",
    "es-EC": "dd/MM/yyyy",
    "ar-KW": "dd/MM/yyyy",
    "en-PH": "M/d/yyyy",
    "es-CL": "dd-MM-yyyy",
    "ar-AE": "dd/MM/yyyy",
    "es-UY": "dd/MM/yyyy",
    "ar-BH": "dd/MM/yyyy",
    "es-PY": "dd/MM/yyyy",
    "ar-QA": "dd/MM/yyyy",
    "es-BO": "dd/MM/yyyy",
    "es-SV": "dd/MM/yyyy",
    "es-HN": "dd/MM/yyyy",
    "es-NI": "dd/MM/yyyy",
    "es-PR": "dd/MM/yyyy",
    "am-ET": "d/M/yyyy",
    "tzm-Latn-DZ": "dd-MM-yyyy",
    "iu-Latn-CA": "d/MM/yyyy",
    "sma-NO": "dd.MM.yyyy",
    "mn-Mong-CN": "yyyy/M/d",
    "gd-GB": "dd/MM/yyyy",
    "en-MY": "d/M/yyyy",
    "prs-AF": "dd/MM/yy",
    "bn-BD": "dd-MM-yy",
    "wo-SN": "dd/MM/yyyy",
    "rw-RW": "M/d/yyyy",
    "qut-GT": "dd/MM/yyyy",
    "sah-RU": "MM.dd.yyyy",
    "gsw-FR": "dd/MM/yyyy",
    "co-FR": "dd/MM/yyyy",
    "oc-FR": "dd/MM/yyyy",
    "mi-NZ": "dd/MM/yyyy",
    "ga-IE": "dd/MM/yyyy",
    "se-SE": "yyyy-MM-dd",
    "br-FR": "dd/MM/yyyy",
    "smn-FI": "d.M.yyyy",
    "moh-CA": "M/d/yyyy",
    "arn-CL": "dd-MM-yyyy",
    "ii-CN": "yyyy/M/d",
    "dsb-DE": "d. M. yyyy",
    "ig-NG": "d/M/yyyy",
    "kl-GL": "dd-MM-yyyy",
    "lb-LU": "dd/MM/yyyy",
    "ba-RU": "dd.MM.yy",
    "nso-ZA": "yyyy/MM/dd",
    "quz-BO": "dd/MM/yyyy",
    "yo-NG": "d/M/yyyy",
    "ha-Latn-NG": "d/M/yyyy",
    "fil-PH": "M/d/yyyy",
    "ps-AF": "dd/MM/yy",
    "fy-NL": "d-M-yyyy",
    "ne-NP": "M/d/yyyy",
    "se-NO": "dd.MM.yyyy",
    "iu-Cans-CA": "d/M/yyyy",
    "sr-Latn-RS": "d.M.yyyy",
    "si-LK": "yyyy-MM-dd",
    "sr-Cyrl-RS": "d.M.yyyy",
    "lo-LA": "dd/MM/yyyy",
    "km-KH": "yyyy-MM-dd",
    "cy-GB": "dd/MM/yyyy",
    "bo-CN": "yyyy/M/d",
    "sms-FI": "d.M.yyyy",
    "as-IN": "dd-MM-yyyy",
    "ml-IN": "dd-MM-yy",
    "en-IN": "dd-MM-yyyy",
    "or-IN": "dd-MM-yy",
    "bn-IN": "dd-MM-yy",
    "tk-TM": "dd.MM.yy",
    "bs-Latn-BA": "d.M.yyyy",
    "mt-MT": "dd/MM/yyyy",
    "sr-Cyrl-ME": "d.M.yyyy",
    "se-FI": "d.M.yyyy",
    "zu-ZA": "yyyy/MM/dd",
    "xh-ZA": "yyyy/MM/dd",
    "tn-ZA": "yyyy/MM/dd",
    "hsb-DE": "d. M. yyyy",
    "bs-Cyrl-BA": "d.M.yyyy",
    "tg-Cyrl-TJ": "dd.MM.yy",
    "sr-Latn-BA": "d.M.yyyy",
    "smj-NO": "dd.MM.yyyy",
    "rm-CH": "dd/MM/yyyy",
    "smj-SE": "yyyy-MM-dd",
    "quz-EC": "dd/MM/yyyy",
    "quz-PE": "dd/MM/yyyy",
    "hr-BA": "d.M.yyyy.",
    "sr-Latn-ME": "d.M.yyyy",
    "sma-SE": "yyyy-MM-dd",
    "en-SG": "d/M/yyyy",
    "ug-CN": "yyyy-M-d",
    "sr-Cyrl-BA": "d.M.yyyy",
    "es-US": "M/d/yyyy"
  };

  return formats[navigator.language] || 'dd/MM/yyyy';
} 

// alert(navigator.language + ": " + getLocaleDateString());
