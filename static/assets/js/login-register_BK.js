
(function($,W,D)
{
    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
        setupFormValidation: function()
        {
            //form validation rules
            $("#login-form").validate({
                rules: {
					username: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 4
                    }
                },
                messages: {
                    username: {
                        required: "Please provide a username"
                    },
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 4 characters long"
                    }
                },
                submitHandler: function(form) {
                    form.submit();
//                    console.log( $('#login-form').serialize() );
                    
                }
            });

            $("#registration-form").validate({
                rules: {
					first_name: {
                        required: true
                    },
					last_name: {
                        required: true
                    },
					dob: {
                        required: true
                    },
					mobile: {
                        required: true,
						digits: true,
						minlength: 10,
                        maxlength: 10
                    },
					password: {
                        required: true,
						minlength: 4
                    },
                    confirm_password: {
                        required: true,
						equalTo: "#password"
                    },
					state: {
                        required: true
                    },
					district: {
                        required: true
                    }
                },
                messages: {
                    first_name: {
                        required: "Please provide a first name"
                    },
					last_name: {
                        required: "Please provide a last name"
                    },
					dob: {
                        required: "Please provide a date of birth"
                    },
					mobile: {
                        required: "Please provide a mobile number"
                    },
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 4 characters long"
                    }
                },
                submitHandler: function(form) {
                    form.submit();
//                    console.log( $('#registration-form').serialize() );
                    
                }
            });

        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($) {
        JQUERY4U.UTIL.setupFormValidation();
    });

})(jQuery, window, document);