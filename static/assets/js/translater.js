
if(sessionStorage.getItem("lang") == undefined) {
    var translator = $('body').translate({lang: "en", t: dict}); //use English
    $('#lang').val("en");
    $('#lang-mobile').val("en");

    $( ".en").each(function( ) {
        $(this).removeClass('en');
    });

}else {
    var translator = $('body').translate({lang: sessionStorage.getItem("lang"), t: dict});
    $('#lang').val(sessionStorage.getItem("lang"));
    $('#lang-mobile').val(sessionStorage.getItem("lang"));

    $( "."+sessionStorage.getItem("lang")).each(function( ) {
        $(this).removeClass(sessionStorage.getItem("lang"));
    });
}

$('#lang').change(function() { 
    var lang = $(this).val();
    sessionStorage.setItem("lang", lang);
    translator.lang(lang);

    $(".static").each(function( ){
        if($(this).hasClass('lang-en')) {
            $(this).addClass('en');
        }else if($(this).hasClass('lang-hn')) {
            $(this).addClass('hn');
        }else if($(this).hasClass('lang-pn')) {
            $(this).addClass('pn');
        }
    });

    $( "."+lang).each(function( ) {
        $(this).removeClass(lang);
    });
});
$('#lang-mobile').change(function() { 
    var lang = $(this).val();
    sessionStorage.setItem("lang", lang);
    translator.lang(lang);

    $(".static").each(function( ){
        if($(this).hasClass('lang-en')) {
            $(this).addClass('en');
        }else if($(this).hasClass('lang-hn')) {
            $(this).addClass('hn');
        }else if($(this).hasClass('lang-pn')) {
            $(this).addClass('pn');
        }
    });

    $( "."+lang).each(function( ) {
        $(this).removeClass(lang);
    });
});


