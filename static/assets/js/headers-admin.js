(function($){ 
  $(document).ready(function() {
    var conditional_field1 = $(".field-thumbnail_file");
    var conditional_field2 = $(".field-document_file");
    var conditional_field3 = $(".field-thumbnail_url");
    var conditional_field4 = $(".field-reference_website");
    var conditional_field5 = $(".field-redirect_url");
    var conditional_field6 = $(".field-iframe_url");
    var conditional_field7 = $(".field-summary");

    conditional_field4.hide();
    conditional_field5.hide();

    $("#id_category").change(function() {
      if ($(this).val() != '') {
        if ($(this).val() === '2') {
          showAll();
          conditional_field1.hide();
          conditional_field2.hide();
          conditional_field3.hide();
          conditional_field4.hide();
          conditional_field5.hide();
        }else if($(this).val() === '3') {
          conditional_field4.show();
          conditional_field5.show();
        }else {
          showAll();
        }
      }

    });
    $("#id_sub_category").change(function() {
      if ($(this).val() != '') {
        if ($(this).val() === '4') {
          showAll();
          conditional_field1.hide();
          conditional_field2.hide();
          conditional_field6.hide();
          conditional_field4.show();
          conditional_field5.show();
        }else if($(this).val() === '2') {
          showAll();
          conditional_field7.hide();
          conditional_field2.hide();
          conditional_field3.hide();
          conditional_field6.hide();
        }else if($(this).val() === '11') {
          conditional_field4.show();
          conditional_field5.show();
        }
        else {
          showAll();
        }
      }
    });

    function showAll() {
      conditional_field1.show();
      conditional_field2.show();
      conditional_field3.show();
      conditional_field4.hide();
      conditional_field5.hide();
      conditional_field6.show();
      conditional_field7.show();
    }

  });
})(django.jQuery);