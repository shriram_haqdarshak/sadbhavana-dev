
var language = ($.cookie('django_language'));

function nameVal(){
  if(language == 'hi') {
    return "कृपया मान्य नाम दर्ज करें";
  }else if(language == 'pa') {
    return "ਕਿਰਪਾ ਕਰਕੇ ਪ੍ਰਮਾਣਿਕ ​​ਨਾਮ ਦਰਜ ਕਰੋ";
  }else{
    return "Please Enter Valid Name.";
  }
}

$.validator.addMethod("letterswithspace", function (value, element) {
  return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
}, nameVal);

function mobileVal(){
  if(language == 'hi') {
    return "कृपया मान्य मोबाइल नंबर दर्ज करें";
  }else if(language == 'pa') {
    return "ਕਿਰਪਾ ਕਰਕੇ ਵੈਧ ਮੋਬਾਈਲ ਨੰਬਰ ਦਾਖਲ ਕਰੋ";
  }else{
    return "Please Enter Valid Mobile Number.";
  }
}

jQuery.validator.addMethod("numberStartsWith", function (value, element) {
  if (/^[6-9]\d{9}$/g.test(value)) {
    return true;
  } else {
    return false;
  };
}, mobileVal);

function dobVal(){
  if(language == 'hi') {
    return "कृपया DD-MM-YYYY प्रारूप में जन्म तिथि मान्य करें";
  }else if(language == 'pa') {
    return "ਕਿਰਪਾ ਕਰਕੇ DD-MM-YYYY ਫਾਰਮੈਟ ਵਿੱਚ ਪ੍ਰਮਾਣਿਕ ​​ਮਿਤੀ ਦਾਖਲ ਕਰੋ";
  }else{
    return "Please Enter Valid Date Of Birth in DD-MM-YYYY format.";
  }
}

$.validator.addMethod("minAge", function (value, element, min) {
  var today = new Date();
  var birthDate = new Date(value);
  var age = today.getFullYear() - birthDate.getFullYear();

  if (age > min + 1) {
    return true;
  }

  var m = today.getMonth() - birthDate.getMonth();

  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }

  return age >= min;
}, dobVal);

function emailVal(){
  if(language == 'hi') {
    return "कृपया मान्य ईमेल पता दर्ज करें";
  }else if(language == 'pa') {
    return "ਕਿਰਪਾ ਕਰਕੇ ਵੈਧ ਈਮੇਲ ਪਤਾ ਦਾਖਲ ਕਰੋ";
  }else{
    return "Please enter valid Email Address.";
  }
}

$.validator.addMethod("validEmail", function (value, element) {
  if (value != "") {
    if (/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
      return true;
    } else {
      return false;
    }
  } else {
    return true;
  }
}, emailVal);


(function ($, W, D) {
  var JQUERY4U = {};
  JQUERY4U.UTIL =
    {
      setupFormValidation: function () {
        //form validation rules
        $("#login-form").validate({
          rules: {
            username: {
              required: true
            },
            password: {
              required: true,
              minlength: 6
            }
          },
          messages: {
            username: {
              required: "Please enter valid Email/Mobile"
            },
            password: {
              required: "Please Enter password",
              minlength: "Your Password Must Be At Least 6 Characters Long"
            }
          },
          submitHandler: function (form) {
            form.submit();
            //                    console.log( $('#login-form').serialize() );

          }
        });

        if(language == 'hi') {
        $("#registration-form").validate({
          rules: {
            first_name: {
              required: true,
              letterswithspace: true
            },
            last_name: {
              required: true,
              letterswithspace: true
            },
            dob: {
              required: true,
              minAge: 1

            },
            gender: {
              required: true

            },
            email: {
              validEmail: true
            },
            mobile: {
              required: true,
              digits: true,
              minlength: 10,
              maxlength: 10,
              numberStartsWith: true
            },
            password1: {
              required: true,
              minlength: 6
            },
            password2: {
              required: true,
              equalTo: "#password"
            },
            state: {
              required: true
            },
            district: {
              required: false
            }
          },
          messages: {
            first_name: {
              required: "कृपया पहला नाम दर्ज करें"
            },
            last_name: {
              required: "कृपया अंतिम नाम दर्ज करें"
            },
            dob: {
              required: "कृपया जन्म तिथि दर्ज करें"
            },
            gender: {
              required: "कृपया लिंग चुनें"
            },
            mobile: {
              required: "कृपया मोबाइल नंबर दर्ज करें"
            },
            password1: {
              required: "कृपया पासवर्ड दर्ज करें",
              minlength: "आपका पासवर्ड कम से कम छः अक्षरों का होना चाहिए"
            },
            password2: {
              required: "कृपया पासवर्ड की पुष्टि करें",
              minlength: "आपका पासवर्ड कम से कम छः अक्षरों का होना चाहिए"
            },
            district: {
              required: "कृपया जिला चुनें"
            },
            state: {
              required: "कृपया राज्य चुनें"
            },
            terms_of_service: {
              required: "कृपया सेवा की शर्तें स्वीकार करें"
            }
          },
          submitHandler: function (form) {
            form.submit();
            // console.log( $('#registration-form').serialize() );

          }
        });
      }else if(language == 'pa') {
        $("#registration-form").validate({
          rules: {
            first_name: {
              required: true,
              letterswithspace: true
            },
            last_name: {
              required: true,
              letterswithspace: true
            },
            dob: {
              required: true,
              minAge: 1

            },
            gender: {
              required: true

            },
            email: {
              validEmail: true
            },
            mobile: {
              required: true,
              digits: true,
              minlength: 10,
              maxlength: 10,
              numberStartsWith: true
            },
            password1: {
              required: true,
              minlength: 6
            },
            password2: {
              required: true,
              equalTo: "#password"
            },
            state: {
              required: true
            },
            district: {
              required: false
            }
          },
          messages: {
            first_name: {
              required: "ਕਿਰਪਾ ਕਰਕੇ ਪਹਿਲਾਂ ਨਾਮ ਦਰਜ ਕਰੋ"
            },
            last_name: {
              required: "ਕਿਰਪਾ ਕਰਕੇ ਆਖਰੀ ਨਾਮ ਦਰਜ ਕਰੋ"
            },
            dob: {
              required: "ਕਿਰਪਾ ਕਰਕੇ ਜਨਮ ਮਿਤੀ ਦਾਖਲ ਕਰੋ"
            },
            gender: {
              required: "ਕਿਰਪਾ ਕਰਕੇ ਲਿੰਗ ਦੀ ਚੋਣ ਕਰੋ"
            },
            mobile: {
              required: "ਕਿਰਪਾ ਕਰਕੇ ਮੋਬਾਈਲ ਨੰਬਰ ਦਾਖਲ ਕਰੋ"
            },
            password1: {
              required: "ਕਿਰਪਾ ਕਰਕੇ ਪਾਸਵਰਡ ਦਰਜ ਕਰੋ",
              minlength: "ਤੁਹਾਡਾ ਪਾਸਵਰਡ ਘੱਟੋ ਘੱਟ 6 ਅੱਖਰਾਂ ਦਾ ਹੋਣਾ ਚਾਹੀਦਾ ਹੈ"
            },
            password2: {
              required: "ਕਿਰਪਾ ਕਰਕੇ ਪਾਸਵਰਡ ਦੀ ਪੁਸ਼ਟੀ ਕਰੋ",
              minlength: "ਤੁਹਾਡਾ ਪਾਸਵਰਡ ਘੱਟੋ ਘੱਟ 6 ਅੱਖਰਾਂ ਦਾ ਹੋਣਾ ਚਾਹੀਦਾ ਹੈ"
            },
            district: {
              required: "ਕਿਰਪਾ ਕਰਕੇ ਜ਼ਿਲ੍ਹਾ ਦੀ ਚੋਣ ਕਰੋ"
            },
            state: {
              required: "ਰਾਜ ਦੀ ਚੋਣ ਕਰੋ ਜੀ"
            },
            terms_of_service: {
              required: "ਕਿਰਪਾ ਕਰਕੇ ਸੇਵਾ ਦੀਆਂ ਸ਼ਰਤਾਂ ਨੂੰ ਸਵੀਕਾਰ ਕਰੋ"
            }
          },
          submitHandler: function (form) {
            form.submit();
            // console.log( $('#registration-form').serialize() );

          }
        });
      }else {
        $("#registration-form").validate({
          rules: {
            first_name: {
              required: true,
              letterswithspace: true
            },
            last_name: {
              required: true,
              letterswithspace: true
            },
            dob: {
              required: true,
              minAge: 1

            },
            gender: {
              required: true

            },
            email: {
              validEmail: true
            },
            mobile: {
              required: true,
              digits: true,
              minlength: 10,
              maxlength: 10,
              numberStartsWith: true
            },
            password1: {
              required: true,
              minlength: 6
            },
            password2: {
              required: true,
              equalTo: "#password"
            },
            state: {
              required: true
            },
            district: {
              required: false
            }
          },
          messages: {
            first_name: {
              required: "Please Enter First Name"
            },
            last_name: {
              required: "Please Enter Last Name"
            },
            dob: {
              required: "Please Enter Date Of Birth"
            },
            gender: {
              required: "Please Select Gender"
            },
            mobile: {
              required: "Please Enter Mobile Number"
            },
            password1: {
              required: "Please Enter Password",
              minlength: "Your Password Must Be At Least 6 Characters Long"
            },
            password2: {
              required: "Please Enter Confirm Password",
              minlength: "Your Password Must Be At Least 6 Characters Long"
            },
            district: {
              required: "Please Select District"
            },
            state: {
              required: "Please Select State"
            },
            terms_of_service: {
              required: "Please Accept Terms Of Service"
            }
          },
          submitHandler: function (form) {
            form.submit();
            // console.log( $('#registration-form').serialize() );

          }
        });
      }

        if(language == 'hi') {
          $("#profile-form").validate({
            rules: {
              first_name: {
                required: true,
                letterswithspace: true
              },
              last_name: {
                required: true,
                letterswithspace: true
              },
              dob: {
                required: true,
                minAge: 1
              },
              gender: {
                required: true
              },
              email: {
                validEmail: true
              },
              mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
                numberStartsWith: true
              },
              password1: {
                minlength: 6
              },
              password2: {
                equalTo: "#password"
              },
              state: {
                required: true
              },
              district: {
                required: false
              }
            },
            messages: {
              first_name: {
                required: "कृपया पहला नाम दर्ज करें"
              },
              last_name: {
                required: "कृपया अंतिम नाम दर्ज करें"
              },
              dob: {
                required: "कृपया जन्म तिथि दर्ज करें"
              },
              gender: {
                required: "कृपया लिंग चुनें"
              },
              mobile: {
                required: "कृपया मोबाइल नंबर दर्ज करें"
              },
              password1: {
                required: "कृपया पासवर्ड दर्ज करें",
                minlength: "आपका पासवर्ड कम से कम छः अक्षरों का होना चाहिए"
              },
              password2: {
                required: "कृपया पासवर्ड की पुष्टि करें",
                minlength: "आपका पासवर्ड कम से कम छः अक्षरों का होना चाहिए"
              },
              district: {
                required: "कृपया जिला चुनें"
              },
              state: {
                required: "कृपया राज्य चुनें"
              },
              terms_of_service: {
                required: "कृपया सेवा की शर्तें स्वीकार करें"
              }
            },
            submitHandler: function (form) {
              form.submit();
              // console.log( $('#registration-form').serialize() );
  
            }
          });
        }else if(language == 'pa') {
          $("#profile-form").validate({
            rules: {
              first_name: {
                required: true,
                letterswithspace: true
              },
              last_name: {
                required: true,
                letterswithspace: true
              },
              dob: {
                required: true,
                minAge: 1
              },
              gender: {
                required: true
              },
              email: {
                validEmail: true
              },
              mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
                numberStartsWith: true
              },
              password1: {
                minlength: 6
              },
              password2: {
                equalTo: "#password"
              },
              state: {
                required: true
              },
              district: {
                required: false
              }
            },
            messages: {
              first_name: {
                required: "ਕਿਰਪਾ ਕਰਕੇ ਪਹਿਲਾਂ ਨਾਮ ਦਰਜ ਕਰੋ"
              },
              last_name: {
                required: "ਕਿਰਪਾ ਕਰਕੇ ਆਖਰੀ ਨਾਮ ਦਰਜ ਕਰੋ"
              },
              dob: {
                required: "ਕਿਰਪਾ ਕਰਕੇ ਜਨਮ ਮਿਤੀ ਦਾਖਲ ਕਰੋ"
              },
              gender: {
                required: "ਕਿਰਪਾ ਕਰਕੇ ਲਿੰਗ ਦੀ ਚੋਣ ਕਰੋ"
              },
              mobile: {
                required: "ਕਿਰਪਾ ਕਰਕੇ ਮੋਬਾਈਲ ਨੰਬਰ ਦਾਖਲ ਕਰੋ"
              },
              password1: {
                required: "ਕਿਰਪਾ ਕਰਕੇ ਪਾਸਵਰਡ ਦਰਜ ਕਰੋ",
                minlength: "ਤੁਹਾਡਾ ਪਾਸਵਰਡ ਘੱਟੋ ਘੱਟ 6 ਅੱਖਰਾਂ ਦਾ ਹੋਣਾ ਚਾਹੀਦਾ ਹੈ"
              },
              password2: {
                required: "ਕਿਰਪਾ ਕਰਕੇ ਪਾਸਵਰਡ ਦੀ ਪੁਸ਼ਟੀ ਕਰੋ",
                minlength: "ਤੁਹਾਡਾ ਪਾਸਵਰਡ ਘੱਟੋ ਘੱਟ 6 ਅੱਖਰਾਂ ਦਾ ਹੋਣਾ ਚਾਹੀਦਾ ਹੈ"
              },
              district: {
                required: "ਕਿਰਪਾ ਕਰਕੇ ਜ਼ਿਲ੍ਹਾ ਦੀ ਚੋਣ ਕਰੋ"
              },
              state: {
                required: "ਰਾਜ ਦੀ ਚੋਣ ਕਰੋ ਜੀ"
              },
              terms_of_service: {
                required: "ਕਿਰਪਾ ਕਰਕੇ ਸੇਵਾ ਦੀਆਂ ਸ਼ਰਤਾਂ ਨੂੰ ਸਵੀਕਾਰ ਕਰੋ"
              }
            },
            submitHandler: function (form) {
              form.submit();
              // console.log( $('#registration-form').serialize() );
  
            }
          });
        }else {
          $("#profile-form").validate({
            rules: {
              first_name: {
                required: true,
                letterswithspace: true
              },
              last_name: {
                required: true,
                letterswithspace: true
              },
              dob: {
                required: true,
                minAge: 1
              },
              gender: {
                required: true
              },
              email: {
                validEmail: true
              },
              mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
                numberStartsWith: true
              },
              password1: {
                minlength: 6
              },
              password2: {
                equalTo: "#password"
              },
              state: {
                required: true
              },
              district: {
                required: false
              }
            },
            messages: {
              first_name: {
                required: "Please Enter First Name"
              },
              last_name: {
                required: "Please Enter Last Name"
              },
              dob: {
                required: "Please Enter Date Of Birth"
              },
              gender: {
                required: "Please Select Gender"
              },
              mobile: {
                required: "Please Enter Mobile Number"
              },
              password1: {
                required: "Please Enter Password",
                minlength: "Your Password Must Be At Least 6 Characters Long"
              },
              password2: {
                required: "Please Enter Confirm Password",
                minlength: "Your Password Must Be At Least 6 Characters Long"
              },
              district: {
                required: "Please Select District"
              },
              state: {
                required: "Please Select State"
              },
              terms_of_service: {
                required: "Please Accept Terms Of Service"
              }
            },
            submitHandler: function (form) {
              form.submit();
              // console.log( $('#registration-form').serialize() );
  
            }
          });
        }

      }
    }

  //when the dom has loaded setup form validation rules
  $(D).ready(function ($) {
    JQUERY4U.UTIL.setupFormValidation();
  });

})(jQuery, window, document);
