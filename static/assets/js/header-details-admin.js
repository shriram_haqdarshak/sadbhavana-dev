(function($){ 
  $(document).ready(function() {
    var conditional_field1 = $("#book_set-group");
    $("#id_sub_category").change(function() {
      if ($(this).val() === '3' || $(this).val() === '9' || $(this).val() === '11') {
        conditional_field1.show();
      }else {
        conditional_field1.hide();
      }
    });
  });
})(django.jQuery);