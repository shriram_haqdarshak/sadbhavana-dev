var numQuestions,navs;

function init() {

		numQuestions = $( ".qButton" ).length;
		navs = $( ".navbutton" );

		show(0);
}

window.onload = init();
var currentQuestion = 0;

$( ".qPanel" ).first().addClass(" showing");
$( ".qButton" ).first().addClass(" current");

function show(i)
{

	$( ".current" ).first().addClass("qButton");
	$( ".showing" ).first().addClass("qPanel");

	$(".qPanel").removeClass("showing");
	$(".qPanel").each( function (index) {
		if(index == i) {
			$(this).addClass("showing");
		}
	});

	$(".qButton").removeClass("current");
	$(".qButton").each( function (index) {
		
		if(index == i) {
			$(this).addClass("current");
		}
	});

	//navs[0].style.cursor = i === numQuestions - 1 ? 'not-allowed' : '';

	if(i === numQuestions - 1) {
		//navs[0].style.cursor = 'not-allowed';
		$(".navbutton").removeAttr("onclick");
		$(".navbutton").attr("onclick",'finishQuiz()');
		$(".navbutton").text('FINISH');
	}else{
		navs[0].style.cursor = '';
	}
	




	//navs[1].style.cursor = i === numQuestions - 1 ? 'not-allowed' : '';
	
	currentQuestion = i;
	
}
function nav(offset)
{
	$(".options").attr('onclick','checkForAnswer(event)');
	$(".options").css('cursor','pointer');

	var targetQuestion = currentQuestion + offset;
	if ( targetQuestion >= 0 && targetQuestion < $( ".qButton" ).length)
		show(targetQuestion);
	
}

function finishQuiz() {
	console.log('Quiz finished');
}

function checkForAnswer(event) {
	$('.options').css('background-color', '');
	$('.options').css('color', '');
	

	if(event.target.htmlFor == '1-Option A') {
		$(event.target).css('background-color', '#008000');
		$(event.target).css('color', '#fff');
	}else if(event.target.htmlFor == '2-Option C') {
		$(event.target).css('background-color', '#008000');
		$(event.target).css('color', '#fff');
	}else if(event.target.htmlFor == 'op3-3') {
		$(event.target).css('background-color', '#008000');
		$(event.target).css('color', '#fff');
	}else if(event.target.htmlFor == 'op4-2') {
		$(event.target).css('background-color', '#008000');
		$(event.target).css('color', '#fff');
	}else if(event.target.htmlFor == 'op5-3') {
		$(event.target).css('background-color', '#008000');
		$(event.target).css('color', '#fff');
	}else if(event.target.htmlFor == 'op6-1') {
		$(event.target).css('background-color', '#008000');
		$(event.target).css('color', '#fff');
	}else if(event.target.htmlFor == 'op7-3') {
		$(event.target).css('background-color', '#008000');
		$(event.target).css('color', '#fff');
	}else if(event.target.htmlFor == 'op8-2') {
		$(event.target).css('background-color', '#008000');
		$(event.target).css('color', '#fff');
	}else if(event.target.htmlFor == 'op9-4') {
		$(event.target).css('background-color', '#008000');
		$(event.target).css('color', '#fff');
	}else if(event.target.htmlFor == 'op10-4') {
		$(event.target).css('background-color', '#008000');
		$(event.target).css('color', '#fff');
	}else{
		$(event.target).css('background-color', '#FF0000');
		$(event.target).css('color', '#fff');
	}
	$(".options").removeAttr('onclick');
	$(".options").css('cursor','not-allowed');

	setTimeout(function(){

	$( ".options" ).each(function() {
		var quesNo = this.htmlFor.split('-');
		var quesClicked = event.target.htmlFor.split('-');		

		if(quesNo[0] == quesClicked[0] && this.htmlFor == 'op1-1') {
			$(this).css('background-color', '#008000');
			$(this).css('color', '#fff');
			
		}else if(quesNo[0] == quesClicked[0] && this.htmlFor == 'op2-3') {
			$(this).css('background-color', '#008000');
			$(this).css('color', '#fff');
		}else if(quesNo[0] == quesClicked[0] && this.htmlFor == 'op3-3') {
			$(this).css('background-color', '#008000');
			$(this).css('color', '#fff');
		}else if(quesNo[0] == quesClicked[0] && this.htmlFor == 'op4-2') {
			$(this).css('background-color', '#008000');
			$(this).css('color', '#fff');
		}else if(quesNo[0] == quesClicked[0] && this.htmlFor == 'op5-3') {
			$(this).css('background-color', '#008000');
			$(this).css('color', '#fff');
		}else if(quesNo[0] == quesClicked[0] && this.htmlFor == 'op6-1') {
			$(this).css('background-color', '#008000');
			$(this).css('color', '#fff');
		}else if(quesNo[0] == quesClicked[0] && this.htmlFor == 'op7-3') {
			$(this).css('background-color', '#008000');
			$(this).css('color', '#fff');
		}else if(quesNo[0] == quesClicked[0] && this.htmlFor == 'op8-2') {
			$(this).css('background-color', '#008000');
			$(this).css('color', '#fff');
		}else if(quesNo[0] == quesClicked[0] && this.htmlFor == 'op9-4') {
			$(this).css('background-color', '#008000');
			$(this).css('color', '#fff');
		}else if(quesNo[0] == quesClicked[0] && this.htmlFor == 'op10-4') {
			$(this).css('background-color', '#008000');
			$(this).css('color', '#fff');
		}
	});

	}, 1000);
	
}

// $(window).on('beforeunload', function() {
//   $(window).on('unload', function() {
//     window.location.href = 'quizes.html';
//   });

//   return 'Not an empty string';
// });