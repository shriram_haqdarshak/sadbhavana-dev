
(function($){ 
  $(document).ready(function() {

    var user_id = $('#id_user').find(":selected").val();

    $("#id_moderater_status").change(function() {
      if ($(this).val() === 'verified') {
        var status = 'verified';
        var points = 10;
      }else {
        var status = 'block';
        var points = 10;
      }

      $.ajax({
        url: '../../../../../engage/addsadbhavanapoints',
        type: 'POST',
        data: {
          type:'group-created',
          status:status,
          points:points,
          user_id:user_id,
          csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        success: function (response) {
            console.log("success");
        }
    });

    });

    var group_id = $('#id_group').find(":selected").val();

    $("#id_group_performance").change(function() {
      if ($(this).val() === 'yes') {
        var status = 'yes';
        var points = 20;
      }else {
        var status = 'no';
        var points = 20;
      }

      $.ajax({
        url: '../../../../../engage/addsadbhavanapoints',
        type: 'POST',
        data: {
          type:'group-performance',
          status:status,
          points:points,
          group_id:group_id,
          csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        success: function (response) {
            console.log("success");
        }
    });

    });

  });
})(django.jQuery);