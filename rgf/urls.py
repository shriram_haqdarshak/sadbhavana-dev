"""rgf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf.urls import include, url
from django.contrib.auth import views as auth_views

from django.views.generic.base import TemplateView
from app.views.home import signup, profile, activate,savefeedback,autocompleteModel,HomeContent,applyforcompetition,precompetition

from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve
from app.forms import EmailValidationOnForgotPassword
from engage.views import autocompleteFriends

admin.site.index_title = "Welcome to Sadbhavana Portal"

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('', include('app.urls')),
    path('content/', include('content.urls')),
    path('engage/', include("engage.urls")),
    path('feedback/', savefeedback, name='feedback'),
    path('search-results/', autocompleteModel, name='search-results'),
    path('admin/', admin.site.urls),
    url(r'^$', HomeContent, name='home'),
    url(r'home', HomeContent, name='home'),
    url(r'^ajax_calls/search/', autocompleteFriends),
    url(r'resources', TemplateView.as_view(template_name='resources.html'), {'GATrackingId': settings.GA_TRACKING_ID}, name='resources'),
    path('apply-for-competition', applyforcompetition, name='apply-for-competition'),
    path('pre-competition', precompetition, name='pre-competition'),
    path('summernote/', include('django_summernote.urls')),

    # TEMPORARY hard-coded links
    url(r'view-competition-winners', TemplateView.as_view(template_name='view-competition-winners.html'), {'GATrackingId': settings.GA_TRACKING_ID}, name='view-competition-winners'),

    # END TEMPORARY hard-coded links

    path('signup/', signup, name='signup'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', activate, name='activate'),
    url(r'^login/$', auth_views.LoginView.as_view(template_name="registration/login.html"), name="login"),
    url(r'logout/$', auth_views.LogoutView.as_view(template_name='registration/login.html'), name="logout"),
    url(r'password_change/$', auth_views.PasswordChangeView.as_view(template_name='registration/password_change.html',success_url='/password_change_done'), name="password_change"),
    url(r'password_change_done/',  auth_views.PasswordChangeDoneView.as_view(template_name='registration/password_change_done.html'), name="password_change_done"),
    url(r'password_reset/$', auth_views.PasswordResetView.as_view(form_class=EmailValidationOnForgotPassword,template_name='registration/password_reset.html',email_template_name='registration/password_reset_email.html',subject_template_name='registration/password_reset_subject.txt',success_url='/password_reset_done/',from_email='support@yoursite.ma'), name="password_reset"),
    url(r'password_reset_done/', auth_views.PasswordResetDoneView.as_view(template_name='registration/password_reset_done.html'), name="password_reset_done"),
    url(r'password_reset_confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',auth_views.PasswordResetConfirmView.as_view(template_name='registration/password_reset_confirm.html',success_url='/password_reset_complete/'), name="password_reset_confirm"),
    url(r'password_reset_complete/', auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete.html'), name="password_reset_complete"),
    url(r'profile', profile, name='profile'),
    url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)