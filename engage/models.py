from django.db import models
from app.models import User
from django_mysql.models import EnumField
from django.core.validators import validate_email

# Create your models here.

class About(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    workplace = models.CharField(max_length=800, default="", blank=True)
    personal_skills = models.CharField(max_length=800, default="", blank=True)
    school_university = models.CharField(max_length=800, default="", blank=True)
    alternate_mobile = models.CharField(max_length=15, blank=True)
    alternate_email = models.EmailField(max_length=254, blank=True, unique=False, validators=[validate_email])
    languages = models.CharField(max_length=400, default="", blank=True)
    photo = models.ImageField(upload_to="engage/profile/", blank=True, verbose_name='Profile picture')
    about = models.CharField(max_length=400, default="", blank=True)
    nick_names = models.CharField(max_length=400, default="", blank=True)
    favourite_quotes = models.CharField(max_length=800, default="", blank=True)
    contact_info = EnumField(
        choices=['private', 'public'], default="private", blank=True)
    personal_info = EnumField(
        choices=['private', 'public'], default="private", blank=True)
    other_info = EnumField(
        choices=['private', 'public'], default="private", blank=True)
    profile_photo_moderation = EnumField( choices=['verified', 'block'], default="block")

    def user_name(self):
        return self.user.first_name+ " "+self.user.last_name

class FriendRequests(models.Model):
    requester = models.ForeignKey(User, on_delete=models.CASCADE)
    responser = models.CharField(max_length=400, default="")
    request_status = EnumField(
        choices=['Pending', 'Friends'])
    request_date = models.DateTimeField('request date')

class EngageGroups(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=800, default="")
    description = models.CharField(max_length=800, default="")
    privacy = EnumField(
        choices=['private', 'public'], default="", blank=True)
    post_privacy = EnumField(
        choices=['admin', 'member'], default="", blank=True)
    photo = models.ImageField(upload_to="engage/groups/", blank=True, verbose_name='group picture')
    created_date = models.DateTimeField('request date')
    moderater_status = EnumField(choices=['verified', 'block'], default="block")

    def __str__(self):
        return self.name

class Friends(models.Model):
    requester = models.ForeignKey(User, on_delete=models.CASCADE)
    responser = models.CharField(max_length=400, default="")
    request_status = EnumField(
        choices=['Pending', 'Friends'], default="Pending")
    request_date = models.DateTimeField('request date')

class UserPosts(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=800, default="", blank=True)
    body = models.TextField(blank=True, verbose_name='Main Text/Body', help_text='Post Body')
    media_file = models.FileField(upload_to='engage/posts/user/', blank=True)
    pub_date = models.DateTimeField('Published date')
    likes_count = models.SmallIntegerField(default=0)
    comments_count = models.SmallIntegerField(default=0)
    moderater_status = EnumField( choices=['verified', 'block'], default="block")

    def __str__(self):
        return self.title

    def user_name(self):
        return self.user.first_name+ " "+self.user.last_name

class GroupMembers(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(EngageGroups, on_delete=models.CASCADE)
    joining_date = models.DateTimeField('Group joining date')

class GroupPosts(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(EngageGroups, on_delete=models.CASCADE, default="")
    title = models.CharField(max_length=800, default="", blank=True)
    body = models.TextField(blank=True, verbose_name='Main Text/Body', help_text='Post Body')
    media_file = models.FileField(upload_to='engage/posts/user/', blank=True)
    post_privacy = EnumField( choices=['public', 'private'])
    pub_date = models.DateTimeField('Published date')
    likes_count = models.SmallIntegerField(default=0)
    comments_count = models.SmallIntegerField(default=0)
    moderater_status = EnumField( choices=['verified', 'block'], default="block")

    def __str__(self):
        return self.title

    def group_name(self):
        return self.group.name

    def user_name(self):
        return self.user.first_name+ " "+self.user.last_name


class UserPostsComments(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(UserPosts, on_delete=models.CASCADE)
    comment_text = models.TextField(blank=True, verbose_name='Comment', help_text='Comment text')
    commented_date = models.DateTimeField('Commented date')
    moderater_status = EnumField( choices=['verified', 'block'], default="block")
    is_read = EnumField( choices=['yes', 'no'], default="no")

    def __str__(self):
        return self.comment_text

    def post_name(self):
        return self.post.body

    def user_name(self):
        return self.user.first_name+ " "+self.user.last_name

class UserPostsLikes(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(UserPosts, on_delete=models.CASCADE)
    liked_date = models.DateTimeField('Commented date')
    is_read = EnumField( choices=['yes', 'no'], default="no")

class GroupPostsLikes(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(GroupPosts, on_delete=models.CASCADE)
    liked_date = models.DateTimeField('Commented date')
    is_read = EnumField( choices=['yes', 'no'], default="no")

class GroupPostsComments(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(GroupPosts, on_delete=models.CASCADE)
    comment_text = models.TextField(blank=True, verbose_name='Comment', help_text='Comment text')
    commented_date = models.DateTimeField('Commented date')
    moderater_status = EnumField( choices=['verified', 'block'], default="block")
    is_read = EnumField( choices=['yes', 'no'], default="no")
    my = models.CharField(max_length=800, default="")

    def __str__(self):
        return self.comment_text

    def post_name(self):
        return self.post.body

    def user_name(self):
        return self.user.first_name+ " "+self.user.last_name

class Competitions(models.Model):
    name = models.CharField(max_length=800, default="")
    short_description = models.TextField(blank=True, verbose_name='short description', help_text='Competition short description')
    description = models.TextField(blank=True, verbose_name='description', help_text='Competition description')
    banner = models.ImageField(upload_to="engage/competition/", blank=True, verbose_name='competition winner picture')

    def __str__(self):
        return self.name

class CompetitionWinners(models.Model):
    comp = models.ForeignKey(Competitions, on_delete=models.CASCADE)
    competition_type = models.CharField(max_length=800, default="")
    row_group = models.SmallIntegerField(default=0)
    winning_place = models.SmallIntegerField(default=0)
    winner_name = models.CharField(max_length=800, default="")
    age = models.SmallIntegerField(default=0)
    photo = models.ImageField(upload_to="engage/competition/", blank=True, verbose_name='competition winner picture')

class SadbhavanaScore(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    points = models.SmallIntegerField(default=0)

    def user_name(self):
        return self.user.first_name+ " "+self.user.last_name

class GroupPerformance(models.Model):
    group = models.ForeignKey(EngageGroups, on_delete=models.CASCADE)
    group_performance = EnumField( choices=['yes', 'no'], default="no", verbose_name='Is group performing well?')

    def get_group_name(self):
        return self.group.name

class ReportUser(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE, null=True, related_name='user')
    reporter = models.ForeignKey(User,on_delete=models.CASCADE, null=True, related_name='reporter')

    def get_user_name(self):
        return self.user.first_name+ " "+self.user.last_name

    def get_reporter_name(self):
        return self.reporter.first_name+ " "+self.reporter.last_name

class Moderation(models.Model):
    moderation_status = EnumField(
        choices=['Enable', 'Disable'])