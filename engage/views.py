from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import About, Friends, FriendRequests, UserPosts, EngageGroups, GroupMembers, UserPosts, GroupPosts, UserPostsComments, UserPostsLikes, GroupPostsLikes, GroupPostsComments, Competitions, CompetitionWinners, SadbhavanaScore, ReportUser, Moderation
from app.models import User
from django.conf import settings
from datetime import datetime
from django.http import JsonResponse, HttpResponse, HttpResponseForbidden
from engage.forms import ImageFileUploadForm, ImageUploadForm, UserPostForm, GroupPostForm, ImageUploadUpdateForm
import json
from django.db.models import F
import mimetypes
from django.db.models.query import QuerySet

# Create your views here.

# Get moderation status
mod = Moderation.objects.get()
if mod.moderation_status == 'Enable':
  moderater_status = 'block'
else:
  moderater_status = 'verified'

# get user profile and post details
@login_required
def getprofile(request):

  user_id = request.user.id

  # get user details
  q = """ SELECT au.*,ea.about,ea.photo,ea.profile_photo_moderation FROM app_user au
      LEFT JOIN engage_about ea ON ea.user_id=au.id
      WHERE au.id = {} """.format(user_id)
  profiledetails = User.objects.raw(q)

  # get user post details
  d = """ SELECT * FROM engage_userposts WHERE user_id = {} AND moderater_status = 'verified' ORDER BY id DESC """.format(user_id)
  userposts = UserPosts.objects.raw(d)

  # get the media file format
  for posts in userposts:
    file_mime = mimetypes.guess_type(str(posts.media_file))

    if file_mime[0] == 'image/jpeg' or file_mime[0] == 'image/png' :
      mediaFormat = 'image'
    elif file_mime[0] == 'video/mp4' or file_mime[0] == 'video/3gpp' or file_mime[0] == 'video/webm':
      mediaFormat = 'video'
    else:
      mediaFormat = 'unsupported'

    posts.mediaFormat = mediaFormat

  form = UserPostForm()

  if profiledetails[0].dob:
    age = int((datetime.now().date() - profiledetails[0].dob).days / 365.25)
    profiledetails[0].age = age

  context = {
    'baseURL': settings.BASE_URL,
    'profiledetails': profiledetails[0],
    'userposts': userposts,
    'userId': user_id,
    'navBar': 'profile-engage',
    'form': form
  }

  return render(request, 'engage/profile.html', context)

# Get other users profile and posts details
@login_required
def userprofile(request, header_id):
  user_id = header_id

  # get status if login is user friend with other user
  c = """ SELECT * FROM engage_friends WHERE requester_id = {} AND responser = {} """.format(request.user.id, user_id)
  isalreadyfriend = Friends.objects.raw(c)

  # get user profile details
  q = """ SELECT au.*,ea.* FROM app_user au
          LEFT JOIN engage_about ea ON ea.user_id=au.id WHERE au.id = {} """.format(user_id)
  profiledetails = User.objects.raw(q)

  if profiledetails[0].dob:
    age = int((datetime.now().date() - profiledetails[0].dob).days / 365.25)
    profiledetails[0].age = age

  # get user profile details
  d = """ SELECT * FROM engage_userposts WHERE user_id = {} AND moderater_status = 'verified' """.format(user_id)
  userposts = UserPosts.objects.raw(d)

  # get the media file format
  for posts in userposts:
    file_mime = mimetypes.guess_type(str(posts.media_file))

    if file_mime[0] == 'image/jpeg' or file_mime[0] == 'image/png' :
      mediaFormat = 'image'
    elif file_mime[0] == 'video/mp4' or file_mime[0] == 'video/3gpp' or file_mime[0] == 'video/webm':
      mediaFormat = 'video'
    else:
      mediaFormat = 'unsupported'

    posts.mediaFormat = mediaFormat


  context = {
    'baseURL': settings.BASE_URL,
    'profiledetails': profiledetails[0],
    'isalreadyfriend': isalreadyfriend,
    'userposts': userposts,
    'friendid': header_id,
    'userId': request.user.id,
    'navBar': 'user-profile'
  }

  return render(request, 'engage/user-profile.html', context)

# get content/details for home page
@login_required
def gethome(request):
  user_id = request.user.id
  userposts = []

  # check user friends with
  c = """ SELECT responser FROM engage_friends WHERE requester_id = {} AND request_status = 'Friends' """.format(request.user.id)

  # get users friend posts
  d = """ SELECT *,ea.photo as profile_picture,eup.id as post_id,ea.profile_photo_moderation FROM engage_userposts eup
      LEFT JOIN app_user au ON au.id = eup.user_id
      LEFT JOIN engage_about ea ON ea.user_id = au.id
      WHERE eup.user_id IN({}) OR eup.user_id = {} AND eup.moderater_status = 'verified' ORDER BY eup.id DESC """.format(c,request.user.id)
  userposts1 = UserPosts.objects.raw(d)

  # check user friends of
  e = """ SELECT requester_id FROM engage_friends WHERE responser = {} AND request_status = 'Friends' """.format(request.user.id)

  # get users friend posts
  f = """ SELECT *,ea.photo as profile_picture,eup.id as post_id, ea.profile_photo_moderation FROM engage_userposts eup
      LEFT JOIN app_user au ON au.id = eup.user_id
      LEFT JOIN engage_about ea ON ea.user_id = au.id
      WHERE eup.moderater_status = 'verified' AND eup.user_id IN({}) ORDER BY eup.id DESC """.format(e,request.user.id)
  userposts2 = UserPosts.objects.raw(f)

  for x in userposts1:
    userposts.append(x)

  for x in userposts2:
    userposts.append(x)

  # get the media file format
  for posts in userposts:
    file_mime = mimetypes.guess_type(str(posts.media_file))

    if file_mime[0] == 'image/jpeg' or file_mime[0] == 'image/png' :
      mediaFormat = 'image'
    elif file_mime[0] == 'video/mp4' or file_mime[0] == 'video/3gpp' or file_mime[0] == 'video/webm':
      mediaFormat = 'video'
    else:
      mediaFormat = 'unsupported'

    posts.mediaFormat = mediaFormat

  form = UserPostForm()

  context = {
      'navBar': 'home-engage',
      'userposts': userposts,
      'form': form,
      'userId': user_id
  }
  return render(request, 'engage/home.html', context)

# Get all groups
@login_required
def getgroups(request):

  user_id = request.user.id

  q = """ SELECT * FROM engage_engagegroups WHERE moderater_status = 'verified' """.format()

  groupdetails = EngageGroups.objects.raw(q)

  context = {
    'baseURL': settings.BASE_URL,
    'groupdetails': groupdetails,
    'navBar': 'groups-engage'
  }

  return render(request, 'engage/groups.html', context)

# get groups joint by login user
@login_required
def getmygroups(request):

  user_id = request.user.id

  # get groups joint by user
  q = """ SELECT * FROM engage_engagegroups eg
      JOIN engage_groupmembers egm ON egm.group_id = eg.id
      WHERE egm.user_id = {} AND WHERE moderater_status = 'verified' """.format(user_id)

  mygroupdetails = EngageGroups.objects.raw(q)

  # get groups created by user
  s = """ SELECT * FROM engage_engagegroups WHERE user_id = {} AND moderater_status = 'verified'  """.format(user_id)

  owngroupdetails = EngageGroups.objects.raw(s)

  context = {
    'baseURL': settings.BASE_URL,
    'mygroupdetails': mygroupdetails,
    'owngroupdetails': owngroupdetails,
    'navBar': 'my-groups-engage'
  }

  return render(request, 'engage/my-groups.html', context)

# get post details
@login_required
def singlepost(request, post_id, not_type=None,not_id=None):
  user_id = request.user.id

  # check if user clicked from notification and update read status
  if not_type != None and not_type == 'comment' :
    UserPostsComments.objects.filter(id=not_id).update(is_read='yes')

  # check if user clicked from notification and update read status
  if not_type != None and not_type == 'like' :
    UserPostsLikes.objects.filter(id=not_id).update(is_read='yes')

  if user_id :
    # get current user details
    u = """ SELECT * FROM app_user au
            LEFT JOIN engage_about ea ON ea.user_id=au.id
            WHERE au.id = {} """.format(user_id)
    currentuserdetails = User.objects.raw(u)

    # get post details
    q = """ SELECT * FROM engage_userposts eup
            LEFT JOIN app_user au ON au.id=eup.user_id
            LEFT JOIN engage_about ea ON ea.user_id=au.id
            WHERE eup.id = {} """.format(post_id)
    singlepostdetails = UserPosts.objects.raw(q)

    # get the media file format
    for details in singlepostdetails:
      file_mime = mimetypes.guess_type(str(details.media_file))

    if file_mime[0] == 'image/jpeg' or file_mime[0] == 'image/png' :
      mediaFormat = 'image'
    elif file_mime[0] == 'video/mp4' or file_mime[0] == 'video/3gpp' or file_mime[0] == 'video/webm':
      mediaFormat = 'video'
    else:
      mediaFormat = 'unsupported'

    # get likes on post
    w = """ SELECT *, ea.photo, ea.profile_photo_moderation FROM engage_userpostslikes upl
            left JOIN app_user au ON au.id=upl.user_id
            left JOIN engage_about ea ON ea.user_id=au.id
            WHERE upl.post_id = {} """.format(post_id)
    postLikes = UserPostsLikes.objects.raw(w)

    # get comments on post and count
    r = """ SELECT * FROM engage_userpostscomments epc
            LEFT JOIN app_user au ON au.id=epc.user_id
            LEFT JOIN engage_about ea ON ea.user_id=au.id
            WHERE epc.post_id = {} AND epc.moderater_status = 'verified' ORDER BY epc.id DESC """.format(post_id)
    postcomments = UserPosts.objects.raw(r)
    commentscount = sum(1 for comments in postcomments)

    # get likes count
    s = """ SELECT * FROM engage_userpostslikes WHERE post_id = {} """.format(post_id)
    postlikes = UserPostsLikes.objects.raw(s)
    likescount = sum(1 for likes in postlikes)

    # check user self liked post
    t = """ SELECT * FROM engage_userpostslikes WHERE post_id = {} AND user_id ={} """.format(post_id, user_id)
    iscurrentuserlike = UserPostsLikes.objects.raw(t)

    context = {
        'baseURL': settings.BASE_URL,
        'navBar': 'post-single',
        'post_id': post_id,
        'singlepostdetails': singlepostdetails[0],
        'postcomments': postcomments,
        'commentscount': commentscount,
        'likescount': likescount,
        'iscurrentuserlike': iscurrentuserlike,
        'currentuserdetails': currentuserdetails[0],
        'postLikes' : postLikes,
        'mediaFormat': mediaFormat,
        'userId': user_id
    }
    return render(request, 'engage/post-single.html', context)
  else:
    # if user not login
    # get post details
    q = """ SELECT * FROM engage_userposts eup
        LEFT JOIN app_user au ON au.id=eup.user_id
        LEFT JOIN engage_about ea ON ea.user_id=au.id
        WHERE eup.id = {} """.format(post_id)

    singlepostdetails = UserPosts.objects.raw(q)
    context = {
        'baseURL': settings.BASE_URL,
        'navBar': 'post-single',
        'post_id': post_id,
        'singlepostdetails': singlepostdetails[0],
        'postcomments': '',
        'commentscount': '',
        'likescount': '',
        'iscurrentuserlike': '',
        'currentuserdetails': '',
        'mediaFormat': '',
        'userId': user_id
    }
    return render(request, 'engage/post-single.html', context)

# get group post details
@login_required
def singlegrouppost(request, post_id, not_type=None,not_id=None):
  user_id = request.user.id

  # check if user clicked from notification and update read status
  if not_type != None and not_type == 'comment' :
    GroupPostsComments.objects.filter(id=not_id).update(is_read='yes')

  # check if user clicked from notification and update read status
  if not_type != None and not_type == 'like' :
    GroupPostsLikes.objects.filter(id=not_id).update(is_read='yes')

  if user_id :
    # get current user details
    u = """ SELECT * FROM app_user au
            LEFT JOIN engage_about ea ON ea.user_id=au.id
            WHERE au.id = {} """.format(user_id)
    currentuserdetails = User.objects.raw(u)

    # get group post details
    q = """ SELECT * FROM engage_groupposts egp
            LEFT JOIN app_user au ON au.id=egp.user_id
            LEFT JOIN engage_about ea ON ea.user_id=au.id
            WHERE egp.id = {} ORDER BY egp.id DESC""".format(post_id)
    singlepostdetails = GroupPosts.objects.raw(q)

    # get the media file format
    for details in singlepostdetails:
      file_mime = mimetypes.guess_type(str(details.media_file))

    if file_mime[0] == 'image/jpeg' or file_mime[0] == 'image/png':
      mediaFormat = 'image'
    elif file_mime[0] == 'video/mp4' or file_mime[0] == 'video/3gpp' or file_mime[0] == 'video/webm':
      mediaFormat = 'video'
    else:
      mediaFormat = 'unsupported'

    # get comments on group post and count
    r = """ SELECT * FROM engage_grouppostscomments epc
            LEFT JOIN app_user au ON au.id=epc.user_id
            LEFT JOIN engage_about ea ON ea.user_id=au.id
            WHERE epc.post_id = {} AND epc.moderater_status = 'verified' ORDER BY epc.id DESC """.format(post_id)
    postcomments = GroupPosts.objects.raw(r)
    commentscount = sum(1 for comments in postcomments)

    # get likes on group post
    w = """ SELECT *, ea.photo, ea.profile_photo_moderation FROM engage_grouppostslikes epl
        JOIN app_user au ON au.id=epl.user_id
        JOIN engage_about ea ON ea.user_id=au.id
        WHERE epl.post_id = {} """.format(post_id)
    totalPostLikes = GroupPostsLikes.objects.raw(w)

    # get likes count on group post
    s = """ SELECT * FROM engage_grouppostslikes WHERE post_id = {} """.format(post_id)
    postlikes = GroupPostsLikes.objects.raw(s)
    likescount = sum(1 for likes in postlikes)

    # check user self liked post
    t = """ SELECT * FROM engage_grouppostslikes WHERE post_id = {} AND user_id ={} """.format(post_id, user_id)
    iscurrentuserlike = GroupPostsLikes.objects.raw(t)

    context = {
        'navBar': 'post-single',
        'post_id': post_id,
        'singlepostdetails': singlepostdetails[0],
        'postcomments': postcomments,
        'commentscount': commentscount,
        'likescount': likescount,
        'userId': user_id,
        'iscurrentuserlike': iscurrentuserlike,
        'currentuserdetails': currentuserdetails[0],
        'totalPostLikes' : totalPostLikes,
        'mediaFormat': mediaFormat
    }
    return render(request, 'engage/group-post-single.html', context)
  else :
    # if user not login
    # get post details
    q = """ SELECT * FROM engage_groupposts egp
        LEFT JOIN app_user au ON au.id=egp.user_id
        LEFT JOIN engage_about ea ON ea.user_id=au.id
        WHERE egp.id = {} ORDER BY egp.id DESC""".format(post_id)
    singlepostdetails = GroupPosts.objects.raw(q)
    context = {
        'navBar': 'post-single',
        'post_id': post_id,
        'singlepostdetails': singlepostdetails[0],
        'postcomments': '',
        'commentscount': '',
        'likescount': '',
        'userId': '',
        'iscurrentuserlike': '',
        'currentuserdetails': '',
        'mediaFormat': ''
    }
    return render(request, 'engage/group-post-single.html', context)

# get friends of login user
@login_required
def allFriends(request):

  user_id = request.user.id
  friendsdetails = []

  # get user friends of
  a = """ SELECT requester_id FROM engage_friends WHERE responser= {} AND request_status='Friends' """.format(user_id)

  q = """ SELECT au.*,ea.*, au.id as friend_id,eru.reporter_id FROM app_user au
          LEFT JOIN engage_about ea ON au.id=ea.user_id
          LEFT JOIN engage_reportuser eru ON au.id=eru.user_id
          WHERE au.id IN({}) """.format(a)

  friendsdetails1 = User.objects.raw(q)

  # get user friends with
  s = """ SELECT responser FROM engage_friends WHERE requester_id= {} AND request_status='Friends' """.format(user_id)

  t = """ SELECT au.*,ea.*, au.id as friend_id,eru.reporter_id FROM app_user au
          LEFT JOIN engage_about ea ON au.id=ea.user_id
          LEFT JOIN engage_reportuser eru ON au.id=eru.user_id
          WHERE au.id IN({}) """.format(s)

  friendsdetails2 = User.objects.raw(t)

  for x in friendsdetails1:
    friendsdetails.append(x)

  for x in friendsdetails2:
    friendsdetails.append(x)

  # get pending request
  b = """ SELECT requester_id FROM engage_friends WHERE responser= {} AND request_status='Pending' """.format(user_id)

  r = """ SELECT au.*,ea.*, au.id as friend_id,eru.reporter_id FROM app_user au
          LEFT JOIN engage_about ea ON au.id=ea.user_id
          LEFT JOIN engage_reportuser eru ON au.id=eru.user_id
          WHERE au.id IN({}) """.format(b)

  friendsPending = User.objects.raw(r)

  friendsCount = 0

  for friends in friendsdetails:
    friendsCount = friendsCount + 1

  pendingCount = 0

  for friends in friendsPending:
    pendingCount = pendingCount + 1

  context = {
    'baseURL': settings.BASE_URL,
    'friendsdetails': friendsdetails,
    'friendsPending': friendsPending,
    'friendsCount': friendsCount,
    'pendingCount': pendingCount,
    'userId': user_id,
    'navBar': 'friends'
  }

  return render(request, 'engage/friends.html', context)

# get group details
@login_required
def singlegroup(request, header_id):

  user_id = request.user.id

  # get group details
  q = """ SELECT * FROM engage_engagegroups WHERE id={} AND moderater_status = 'verified' """.format(header_id)
  groupdetails = EngageGroups.objects.raw(q)

  # get group members details
  r = """ SELECT *,ea.photo as profile_pic,eg.user_id,au.state, eg.group_id, ea.profile_photo_moderation FROM engage_groupmembers eg
          LEFT JOIN app_user au ON au.id = eg.user_id
          LEFT JOIN engage_about ea ON ea.user_id = eg.user_id
          WHERE group_id={} """.format(header_id)
  groupmembers = GroupMembers.objects.raw(r)

  # get count of group members
  membercount = sum(1 for groupmember in groupmembers)

  # check if the user is already member
  s = """ SELECT id FROM engage_groupmembers WHERE user_id = {} AND group_id = {} """.format(user_id,header_id)
  isalreadymember = GroupMembers.objects.raw(s)

  # get the group post details
  q = """ SELECT *, ea.photo as profile_photo, ea.profile_photo_moderation FROM engage_groupposts egp
      LEFT JOIN app_user au ON au.id = egp.user_id
      LEFT JOIN engage_about ea ON ea.user_id = au.id
      WHERE group_id={} AND egp.moderater_status = 'verified' ORDER BY egp.id DESC""".format(header_id)
  groupposts = GroupPosts.objects.raw(q)

  # get the media file format
  for posts in groupposts:
    file_mime = mimetypes.guess_type(str(posts.media_file))

    if file_mime[0] == 'image/jpeg' or file_mime[0] == 'image/png' :
      mediaFormat = 'image'
    elif file_mime[0] == 'video/mp4' or file_mime[0] == 'video/3gpp' or file_mime[0] == 'video/webm':
      mediaFormat = 'video'
    else:
      mediaFormat = 'unsupported'

    posts.mediaFormat = mediaFormat

  form = GroupPostForm()

  context = {
    'navBar': 'group-view',
    'groupId': header_id,
    'groupdetails': groupdetails[0],
    'isalreadymember': isalreadymember,
    'groupmembers': groupmembers,
    'groupposts': groupposts,
    'user_id': user_id,
    'membercount': membercount,
    'form': form
  }
  return render(request, 'engage/group-single.html', context)

# Get create group page
@login_required
def creategroup(request):

  user_id = request.user.id
  form = ImageUploadForm()

  context = {
    'navBar': 'group-create',
    'form': form
  }
  return render(request, 'engage/create-group.html', context)

# create new group
def creategroupajax(request):

  if request.method == 'POST':
      form = ImageUploadForm(request.POST, request.FILES)
      created_date = datetime.now()
      name = request.POST['name']
      description = request.POST['description']
      privacy = request.POST['privacy']
      post_privacy = request.POST['post_privacy']

      if form.is_valid() and len(request.FILES) != 0 and len(request.POST['name']) != 0:
          form = ImageUploadForm(request.POST or None,request.FILES)

          form = EngageGroups(user_id=request.user.id,name=name,description=description,privacy=privacy,post_privacy=post_privacy,created_date=created_date,photo=request.FILES['photo'],moderater_status='verified')
          form.save()

          return HttpResponse('Group Created Successfully.You can view your group after moderaters approval.')

      else:
        nameErr = descriptionErr = privacyErr = postPrivacyErr = photoErr = ''

        if len(request.POST['name']) == 0:
          nameErr = 'Name field is required.'
        if len(request.POST['description']) == 0:
          descriptionErr = 'Description field is required.'
        if len(request.POST['privacy']) == 0:
          privacyErr = 'Privacy field is required.'
        if len(request.POST['post_privacy']) == 0:
          postPrivacyErr = 'Post privacy field is required.'
        if len(request.FILES) == 0:
          photoErr = "Select group photo."


        result_dic = {
            'error': 'Fill all required fields.',
            'name': nameErr,
            'description': descriptionErr,
            'privacy': privacyErr,
            'post_privacy': postPrivacyErr,
            'photo': photoErr
        }
        return JsonResponse(result_dic)

  return HttpResponseForbidden('allowed only via POST')

# get group settings page
@login_required
def groupsettings(request, group_id):

  user_id = request.user.id

  # get group settings details
  d = """ SELECT * FROM engage_engagegroups WHERE user_id = {} AND id = {} AND moderater_status = 'verified' """.format(user_id,group_id)
  groupsettingsdetails = EngageGroups.objects.raw(d)

  form = ImageUploadUpdateForm()

  context = {
    'baseURL': settings.BASE_URL,
    'groupsettingsdetails': groupsettingsdetails[0],
    'groupId': group_id,
    'form': form,
    'navBar': 'group-settings-engage'
  }

  return render(request, 'engage/group-settings.html', context)

# get login users details on about page
@login_required
def aboutengage(request):

    user_id = request.user.id

    # get user about details
    q = """ SELECT ea.*,ea.photo,au.first_name, au.last_name, au.state, au.mobile, au.email, au.dob, au.gender, ea.       profile_photo_moderation
            FROM engage_about ea
            left join app_user au on au.id=ea.user_id
            WHERE ea.user_id = {} """.format(user_id)

    aboutdetails = About.objects.raw(q)

    if not aboutdetails:
      # add instance in engage_about table
      about_instance = About.objects.create(user_id=user_id)
      about_instance.save()

      q = """ SELECT ea.*,ea.photo,au.first_name, au.last_name, au.state, au.mobile, au.email, au.dob, au.gender, ea.profile_photo_moderation
              FROM engage_about ea
              left join app_user au on au.id=ea.user_id
              WHERE ea.user_id = {} """.format(user_id)

      aboutdetails = About.objects.raw(q)

    form = ImageFileUploadForm()

    context = {
      'baseURL': settings.BASE_URL,
      'aboutdetails': aboutdetails[0],
      'navBar': 'about-engage',
      'form': form
    }

    return render(request, 'engage/about-engage.html', context)

# Edit/Update about details of user
def insert(request):

    if(request.POST['columnName'] == 'workplace'):
      About.objects.filter(user_id=request.user.id).update(workplace=request.POST['valueNew'])
    elif(request.POST['columnName'] == 'personal_skills'):
      About.objects.filter(user_id=request.user.id).update(personal_skills=request.POST['valueNew'])
    elif(request.POST['columnName'] == 'school_university'):
      About.objects.filter(user_id=request.user.id).update(school_university=request.POST['valueNew'])
    elif(request.POST['columnName'] == 'alternate_mobile'):
      About.objects.filter(user_id=request.user.id).update(alternate_mobile=request.POST['valueNew'])
    elif(request.POST['columnName'] == 'alternate_email'):
      About.objects.filter(user_id=request.user.id).update(alternate_email=request.POST['valueNew'])
    elif(request.POST['columnName'] == 'languages'):
      About.objects.filter(user_id=request.user.id).update(languages=request.POST['valueNew'])
    elif(request.POST['columnName'] == 'about'):
      About.objects.filter(user_id=request.user.id).update(about=request.POST['valueNew'])
    elif(request.POST['columnName'] == 'nick_names'):
      About.objects.filter(user_id=request.user.id).update(nick_names=request.POST['valueNew'])
    elif(request.POST['columnName'] == 'favourite_quotes'):
      About.objects.filter(user_id=request.user.id).update(favourite_quotes=request.POST['valueNew'])
    elif(request.POST['columnName'] == 'dob'):
      dob = request.POST['columnName'].strftime("%Y-%m-%d")
      User.objects.filter(id=request.user.id).update(dob=dob)
    elif(request.POST['columnName'] == 'gender'):
      User.objects.filter(id=request.user.id).update(gender=request.POST['valueNew'])

    return redirect('/')


# Add/Update user profile picture
def image_upload_ajax(request):
    if request.method == 'POST':

       form = ImageFileUploadForm(request.POST, request.FILES)
       if form.is_valid():

          instance = get_object_or_404(About, user_id=request.user.id)
          form = ImageFileUploadForm(request.POST or None,request.FILES, instance=instance)
          form.save()
          About.objects.filter(user_id=request.user.id).update(profile_photo_moderation='block')
          return JsonResponse({'error': False, 'message': 'Uploaded Successfully'})
       else:
           return JsonResponse({'error': True, 'errors': form.errors})
    else:
        form = ImageFileUploadForm()
        return render(request, 'engage/about-engage.html', {'form': form})


# Edit/Update group settings
def changegroupimage(request):

  if request.method == 'POST':
      form = ImageUploadUpdateForm(request.POST, request.FILES)
      created_date = datetime.now()
      name = request.POST['name']
      description = request.POST['description']
      privacy = request.POST['privacy']
      post_privacy = request.POST['post_privacy']
      group_id = request.POST['group_id']

      if form.is_valid() and len(request.FILES) != 0:

          instance = get_object_or_404(EngageGroups, id=group_id)
          form = ImageUploadUpdateForm(request.POST or None,request.FILES, instance=instance)
          form.save()
          EngageGroups.objects.filter(id=group_id).update(user_id=request.user.id,name=name,description=description,privacy=privacy,post_privacy=post_privacy,created_date=created_date)

          return HttpResponse('Group Settings Updated Successfully.')
      elif form.is_valid() and len(request.FILES) == 0:

          EngageGroups.objects.filter(id=group_id).update(user_id=request.user.id,name=name,description=description,privacy=privacy,post_privacy=post_privacy,created_date=created_date)

          return HttpResponse('Group Settings Updated Successfully.')
      else:

        result_dic = {
            'error': 'Something went wrong.'
        }
        return JsonResponse(result_dic)

  return HttpResponseForbidden('allowed only via POST')

# unfriend user
def unfriend(request):

    Friends.objects.filter(responser=request.POST['friendId']).filter(requester_id=request.user.id).delete()
    Friends.objects.filter(requester_id=request.POST['friendId']).filter(responser=request.user.id).delete()

    return redirect('/')

# Cancel friend request
def cancelrequest(request):

    Friends.objects.filter(responser=request.user.id).filter(requester_id=request.POST['friendId']).delete()

    return redirect('/')

# Accept friend request
def acceptRequest(request):

  user_id = request.user.id
  friend_id = request.POST['friendId']
  request_date = datetime.now()

  Friends.objects.filter(responser=user_id).filter(requester_id=friend_id).update(request_status='Friends')
  return redirect('/')

# Not in use
def autocompleteFriends(request):
    if request.is_ajax():
        q = request.GET.get('search', '')
        search_qs = User.objects.filter(first_name__contains=q)
        results = []

        for r in search_qs:
            results.append(r.first_name)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)

# send friend request
def addfriend(request):

  user_id = request.user.id
  friend_id = request.POST['friendid']
  request_status = 'Pending'
  request_date = datetime.now()

  friends = Friends(requester_id=user_id, request_status=request_status, request_date=request_date, responser=friend_id)

  friends.save()

  return redirect('/')

# join group
def joingroup(request):

  user_id = request.user.id
  group_id = request.POST['groupid']
  joining_date = datetime.now()

  groupmembers = GroupMembers(user_id=user_id, joining_date=joining_date, group_id=group_id)

  groupmembers.save()

  return redirect('/')

# Leave group
def leaveGroup(request):

  user_id = request.user.id
  group_id = request.POST['groupid']

  GroupMembers.objects.filter(user_id=user_id).filter(group_id=group_id).delete()

  return redirect('/')

# Create user post
def createpostajax(request):

  if request.method == 'POST':
      form = ImageUploadForm(request.POST, request.FILES)
      pub_date = datetime.now()
      # title = 'title'
      body = request.POST['desc']
      privacy = 'public'
      #moderater_status = 'verified'

      if form.is_valid() and len(request.FILES) != 0 :
        form = ImageUploadForm(request.POST or None,request.FILES)

        form = UserPosts(user_id=request.user.id,body=body,pub_date=pub_date,media_file=request.FILES['media_file'],moderater_status=moderater_status)
        form.save()

        return HttpResponse('Post Created Successfully. You can view this after moderators approval')

      elif form.is_valid() :

        form = UserPosts(user_id=request.user.id,body=body,pub_date=pub_date,moderater_status=moderater_status)
        form.save()
        return HttpResponse('Post Created Successfully. You can view this after moderators approval')

      else:
        nameErr = descriptionErr = photoErr = ''

        if len(title) == 0:
          nameErr = 'Title field is required.'
        if len(request.POST['desc']) == 0:
          descriptionErr = 'Body field is required.'

        result_dic = {
            'error': 'Fill all required fields.',
            'name': nameErr,
            'description': descriptionErr,
            'photo': photoErr
        }
        return JsonResponse(result_dic)

  return HttpResponseForbidden('allowed only via POST')

# Create group post
def creategrouppostajax(request):

  if request.method == 'POST':
      form = GroupPostForm(request.POST, request.FILES)
      pub_date = datetime.now()
      # title = request.POST['title']
      body = request.POST['desc']
      group_id = request.POST['group_id']
      privacy = 'public'
      #moderater_status = 'verified'

      if form.is_valid() and len(request.FILES) != 0 :
        form = GroupPostForm(request.POST or None,request.FILES)
        form = GroupPosts(user_id=request.user.id,body=body,group_id=group_id,post_privacy=privacy,pub_date=pub_date,media_file=request.FILES['media_file'],moderater_status=moderater_status)
        form.save()

        return HttpResponse('Post Created Successfully. You can view this after moderators approval')

      elif form.is_valid() :
        form = GroupPosts(user_id=request.user.id,body=body,group_id=group_id,post_privacy=privacy,pub_date=pub_date,moderater_status=moderater_status)
        form.save()

        return HttpResponse('Post Created Successfully. You can view this after moderators approval')

      else:
        nameErr = descriptionErr = photoErr = ''

        if len(request.POST['title']) == 0:
          nameErr = 'Title field is required.'
        if len(request.POST['desc']) == 0:
          descriptionErr = 'Body field is required.'


        result_dic = {
            'error': 'Fill all required fields.',
            'name': nameErr,
            'description': descriptionErr,
            'photo': photoErr
        }
        return JsonResponse(result_dic)

  return HttpResponseForbidden('allowed only via POST')

# search error
@login_required
def usernotfound(request):

  context = {
    'baseURL': settings.BASE_URL,
    'navBar': 'user-not-found'
  }

  return render(request, 'engage/search-error.html', context)

# Comment on user post
def commentuserpostajax(request):
  commented_date = datetime.now()
  comment_text = request.POST['comment_text']
  post_id = request.POST['post_id']
  #moderater_status = 'verified'

  form = UserPostsComments(user_id=request.user.id,comment_text=comment_text,commented_date=commented_date,moderater_status=moderater_status,post_id=post_id)
  form.save()
  #increase comments count value in user posts
  UserPosts.objects.filter(id=post_id).update(comments_count=F('comments_count')+1)

  return HttpResponse(form.id)

# like on user post
def likeuserpostajax(request):
  liked_date = datetime.now()
  is_like = request.POST['islike']
  post_id = request.POST['post_id']

  if is_like == 'yes':
    form = UserPostsLikes(user_id=request.user.id,liked_date=liked_date,post_id=post_id)
    form.save()
    #increase likes count value in user posts
    UserPosts.objects.filter(id=post_id).update(likes_count=F('likes_count')+1)

    # Update sadbhavana score table
    points = 1

    objUser = SadbhavanaScore.objects.filter(user_id=request.user.id)

    if objUser:
        SadbhavanaScore.objects.filter(user_id=request.user.id).update(points=F('points')+points)
    else:
        user = SadbhavanaScore(user_id=request.user.id,points=points)
        user.save()

  else:
    UserPostsLikes.objects.filter(user_id=request.user.id).filter(post_id=post_id).delete()
    #descrease likes count value in user posts
    UserPosts.objects.filter(id=post_id).update(likes_count=F('likes_count')-1)

    # Update sadbhavana score table
    points = 1

    objUser = SadbhavanaScore.objects.filter(user_id=request.user.id)

    if objUser:
        SadbhavanaScore.objects.filter(user_id=request.user.id).update(points=F('points')-points)
    else:
        points = 0
        user = SadbhavanaScore(user_id=request.user.id,points=points)
        user.save()

  return HttpResponse('Comment Save Successfully.')

# Comment on group post
def commentgrouppostajax(request):
  commented_date = datetime.now()
  comment_text = request.POST['comment_text']
  post_id = request.POST['post_id']
  #moderater_status = 'verified'

  form = GroupPostsComments(user_id=request.user.id,comment_text=comment_text,commented_date=commented_date,moderater_status=moderater_status,post_id=post_id)
  form.save()
  #increase comments count value in user posts
  GroupPosts.objects.filter(id=post_id).update(comments_count=F('comments_count')+1)

  #return HttpResponse('Comment Save Successfully.')
  return HttpResponse(form.id)

# like on group post
def likegrouppostajax(request):
  liked_date = datetime.now()
  is_like = request.POST['islike']
  post_id = request.POST['post_id']

  if is_like == 'yes':
    form = GroupPostsLikes(user_id=request.user.id,liked_date=liked_date,post_id=post_id)
    form.save()
    #increase likes count value in user posts
    GroupPosts.objects.filter(id=post_id).update(likes_count=F('likes_count')+1)

    # Update sadbhavana score table
    points = 1

    objUser = SadbhavanaScore.objects.filter(user_id=request.user.id)

    if objUser:
        SadbhavanaScore.objects.filter(user_id=request.user.id).update(points=F('points')+points)
    else:
        user = SadbhavanaScore(user_id=request.user.id,points=points)
        user.save()

  else:
    GroupPostsLikes.objects.filter(user_id=request.user.id).filter(post_id=post_id).delete()
    #decrease likes count value in user posts
    GroupPosts.objects.filter(id=post_id).update(likes_count=F('likes_count')-1)

    # Update sadbhavana score table
    points = 1

    objUser = SadbhavanaScore.objects.filter(user_id=request.user.id)

    if objUser:
        SadbhavanaScore.objects.filter(user_id=request.user.id).update(points=F('points')-points)
    else:
        points = 0
        user = SadbhavanaScore(user_id=request.user.id,points=points)
        user.save()

  return HttpResponse('Comment Save Successfully.')

# Delete comment of user
def deleteusercomment(request):

    user_id = request.user.id
    comment_id = request.POST['comment_id']
    post_id = request.POST['post_id']

    UserPostsComments.objects.filter(id=comment_id).filter(user_id=user_id).delete()

    #increase comments count value in user posts
    UserPosts.objects.filter(id=post_id).update(comments_count=F('comments_count')-1)

    return redirect('/')

# Delete comment on group
def deletegroupcomment(request):

    user_id = request.user.id
    comment_id = request.POST['comment_id']
    post_id = request.POST['post_id']

    GroupPostsComments.objects.filter(id=comment_id).filter(user_id=user_id).delete()

    #decrease comments count value in user posts
    GroupPosts.objects.filter(id=post_id).update(comments_count=F('comments_count')-1)

    return redirect('/')

# Delete post of user
def deleteuserpost(request):

    user_id = request.user.id
    post_id = request.POST['post_id']

    UserPosts.objects.filter(id=post_id).filter(user_id=user_id).delete()

    UserPostsComments.objects.filter(id=post_id).delete()
    UserPostsLikes.objects.filter(id=post_id).delete()

    return redirect('/')

# Delete post in group
def deletegrouppost(request):

    user_id = request.user.id
    post_id = request.POST['post_id']

    GroupPosts.objects.filter(id=post_id).filter(user_id=user_id).delete()

    GroupPostsComments.objects.filter(id=post_id).delete()
    GroupPostsLikes.objects.filter(id=post_id).delete()

    return redirect('/')

# Not in use
@login_required
def competitionwinners(request):
  user_id = request.user.id

  d = """ SELECT * FROM engage_competitions ORDER BY id DESC """
  allcompetitions = Competitions.objects.raw(d)

  context = {
      'navBar': 'competition-winners',
      'userId': user_id,
      'allcompetitions': allcompetitions
  }
  return render(request, 'engage/competition-winners.html', context)

# Not in use
@login_required
def singlecompetitionwinner(request, comp_id):
  user_id = request.user.id

  c = """ SELECT * FROM engage_competitionwinners WHERE comp_id = {} """.format(comp_id)
  singlecompetition = CompetitionWinners.objects.raw(c)

  d = """ SELECT * FROM engage_competitions WHERE id = {} """.format(comp_id)
  competitiondetails = Competitions.objects.raw(d)

  e = """ SELECT * FROM engage_competitionwinners WHERE comp_id = {} GROUP BY row_group """.format(comp_id)
  rowtype = CompetitionWinners.objects.raw(e)

  context = {
      'navBar': 'view-competition-winners',
      'userId': user_id,
      'singlecompetition': singlecompetition,
      'rowtype': rowtype,
      'competitiondetails': competitiondetails[0]
  }
  return render(request, 'engage/view-competition-winners.html', context)

# Add sadbhavana points for user
def addsadbhavanapoints(request):

  # Update sadbhavana score table
  if request.POST['type'] == 'group-created':
    status = request.POST['status']
    points = request.POST['points']
    user_id = request.POST['user_id']

    objUser = SadbhavanaScore.objects.filter(user_id=user_id)

    if status == 'verified':
      if objUser:
          SadbhavanaScore.objects.filter(user_id=user_id).update(points=F('points')+points)
      else:
          user = SadbhavanaScore(user_id=user_id,points=points)
          user.save()
    else:
      if objUser:
          SadbhavanaScore.objects.filter(user_id=user_id).update(points=F('points')-points)
      else:
          points = -10
          user = SadbhavanaScore(user_id=user_id,points=points)
          user.save()

  elif request.POST['type'] == 'post-share':
    points = request.POST['points']
    user_id = request.user.id

    objUser = SadbhavanaScore.objects.filter(user_id=user_id)

    if objUser:
      SadbhavanaScore.objects.filter(user_id=user_id).update(points=F('points')+points)
    else:
      user = SadbhavanaScore(user_id=user_id,points=points)
      user.save()

  else:
    status = request.POST['status']
    points = request.POST['points']
    group_id = request.POST['group_id']

    objGroups = EngageGroups.objects.filter(id=group_id)

    user_id = objGroups[0].user_id

    objUser = SadbhavanaScore.objects.filter(user_id=user_id)

    if status == 'yes':
      if objUser:
          SadbhavanaScore.objects.filter(user_id=user_id).update(points=F('points')+points)
      else:
          user = SadbhavanaScore(user_id=user_id,points=points)
          user.save()
    else:
      if objUser:
          SadbhavanaScore.objects.filter(user_id=user_id).update(points=F('points')-points)
      else:
          points = -20
          user = SadbhavanaScore(user_id=user_id,points=points)
          user.save()

  return redirect('/')

# Report user
def reportuser(request):

  user_id = request.user.id
  friend_id = request.POST['friendId']

  reportuser = ReportUser(user_id=friend_id, reporter_id=user_id)
  reportuser.save()

  return redirect('/')

# Delete group
def deletegroup(request):

  user_id = request.user.id
  group_id = request.POST['groupid']

  EngageGroups.objects.filter(id=group_id).delete()

  return redirect('/')

# Change privacy of user
def changePrivacy(request):

  user_id = request.user.id
  columnName = request.POST['columnName']
  valueNew = request.POST['valueNew']

  if columnName == 'contact_info':
    About.objects.filter(user_id=user_id).update(contact_info=valueNew)
  elif columnName == 'personal_info':
    About.objects.filter(user_id=user_id).update(personal_info=valueNew)
  else:
    About.objects.filter(user_id=user_id).update(other_info=valueNew)

  return redirect('/')

# Get likes on user post
def getLikesAjax(request):
  post_id = request.POST['post_id']
  post_likes = []

  w = """ SELECT *, ea.photo, ea.profile_photo_moderation FROM engage_userpostslikes upl
      left JOIN app_user au ON au.id=upl.user_id
      left JOIN engage_about ea ON ea.user_id=au.id
      WHERE upl.post_id = {} """.format(post_id)
  postLikes = UserPostsLikes.objects.raw(w)

  json_res = []
  for record in postLikes:
    json_obj = dict(first_name = record.first_name,last_name = record.last_name,user_id = record.user_id,img = record.photo,profile_photo_moderation=record.profile_photo_moderation)
    json_res.append(json_obj)

  return JsonResponse(json_res, safe=False)

# Get likes on group post
def getGroupPostLikesAjax(request):
  post_id = request.POST['post_id']
  post_likes = []

  w = """ SELECT *, ea.photo, ea.profile_photo_moderation FROM engage_grouppostslikes epl
    JOIN app_user au ON au.id=epl.user_id
    JOIN engage_about ea ON ea.user_id=au.id
    WHERE epl.post_id = {} """.format(post_id)
  totalPostLikes = GroupPostsLikes.objects.raw(w)

  json_res = []
  for record in totalPostLikes:
    json_obj = dict(first_name = record.first_name,last_name = record.last_name,user_id = record.user_id,img = record.photo,profile_photo_moderation = record.profile_photo_moderation)
    json_res.append(json_obj)

  return JsonResponse(json_res, safe=False)