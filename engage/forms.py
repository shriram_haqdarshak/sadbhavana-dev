from django import forms
from engage.models import About, EngageGroups, UserPosts, GroupPosts


class ImageFileUploadForm(forms.ModelForm):
    class Meta:
        model = About
        fields = ('photo',)

class ImageUploadForm(forms.ModelForm):
    class Meta:
        model = EngageGroups
        fields = ('photo',)

class UserPostForm(forms.ModelForm):
    class Meta:
        model = UserPosts
        fields = ('media_file',)

class GroupPostForm(forms.ModelForm):
    class Meta:
        model = GroupPosts
        fields = ('media_file',)

class ImageUploadUpdateForm(forms.ModelForm):
    class Meta:
        model = EngageGroups
        fields = ('photo',)
