from django.contrib import admin
from django.contrib.admin import ModelAdmin
from .models import Competitions, CompetitionWinners, EngageGroups, GroupPerformance, ReportUser, UserPosts, GroupPosts, UserPostsComments, GroupPostsComments, About, Moderation, SadbhavanaScore
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q

# Register your models here.
class CustomCompetitions(ModelAdmin):
  list_display = ['name', 'short_description', 'description']
  list_filter = ('name',)
  list_per_page = 25

class CustomCompetitionWinners(ModelAdmin):
  list_display = ['competition_type', 'winning_place', 'winner_name','age']
  list_filter = ('competition_type',)
  list_per_page = 25

class CustomEngageGroups(ModelAdmin):
  list_display = ['name', 'description', 'moderater_status','created_date']
  list_filter = ('name','moderater_status',)
  list_per_page = 25
  readonly_fields = ["user", "name", "description", "privacy", "post_privacy", "created_date"]
  class Media:
    js = (
        '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', # jquery
        'assets/js/engage-groups-admin.js',
        )

class CustomGroupPerformance(ModelAdmin):
  list_display = [GroupPerformance.get_group_name,'group_performance']
  list_filter = ('group_performance',)
  list_per_page = 25
  class Media:
    js = (
        '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', # jquery
        'assets/js/engage-groups-admin.js',
        )

class CustomReportUser(ModelAdmin):
  list_display = [ReportUser.get_user_name,ReportUser.get_reporter_name]
  list_filter = ('user_id',)
  list_per_page = 25

class CustomModeration(ModelAdmin):
  list_display = ['moderation_status']
  list_per_page = 25

class CustomUserPosts(ModelAdmin):
  list_display = [UserPosts.user_name,'title', 'pub_date']
  list_filter = ('pub_date','user',)
  search_fields = ['user__first_name', 'user__last_name']
  list_per_page = 25
  readonly_fields = ["user", "title", "body", "pub_date", "likes_count", "comments_count"]

class CustomGroupPosts(ModelAdmin):
  list_display = [GroupPosts.group_name,'title', GroupPosts.user_name, 'pub_date']
  list_filter = ('pub_date','group', 'user',)
  list_per_page = 25
  readonly_fields = ["user", "group", "title", "body", "post_privacy", "pub_date","likes_count", "comments_count"]

class CustomUserPostsComments(ModelAdmin):
  list_display = [UserPostsComments.post_name,'comment_text', UserPostsComments.user_name, 'commented_date', 'moderater_status']
  list_filter = ('moderater_status','post', 'user','commented_date',)
  list_per_page = 25
  #list_editable = ('moderater_status',)
  readonly_fields = ["user", "post", "comment_text", "commented_date", "is_read"] 

class CustomGroupPostsComments(ModelAdmin):
  list_display = [GroupPostsComments.post_name,'comment_text', GroupPostsComments.user_name, 'commented_date', 'moderater_status']
  list_filter = ('moderater_status','post', 'user','commented_date',)
  list_per_page = 25
  readonly_fields = ["user", "post", "comment_text", "commented_date", "is_read"]

class ImageListFilter(admin.SimpleListFilter):

    title = _('Has photo')

    parameter_name = 'has_photo'

    def lookups(self, request, model_admin):

        return (
            ('yes', _('Yes')),
            ('no',  _('No')),
        )

    def queryset(self, request, queryset):

        if self.value() == 'yes':
            return queryset.filter(photo__isnull=False).exclude(photo='')

        if self.value() == 'no':
            return queryset.filter(Q(photo__isnull=True) | Q(photo__exact=''))

class CustomAbout(ModelAdmin):
  list_display = [About.user_name,'photo_img']
  list_filter = ('profile_photo_moderation',ImageListFilter,)
  list_per_page = 25
  search_fields = ['user__first_name', 'user__last_name']
  readonly_fields = ["user", "workplace", "personal_skills", "school_university", "alternate_mobile", "alternate_email", "languages", "about", "nick_names", "favourite_quotes", "contact_info", "personal_info", "other_info"]

  def photo_img(self, obj):
      if obj.photo :
      	return mark_safe('<img src="{url}" width="{width}" height={height} alt="No photo" />'.format(
          	url = obj.photo.url,
          	width=100,
          	height=100,
         	)
  )

class CustomSadbhavanaScore(ModelAdmin):
  list_display = [SadbhavanaScore.user_name,'points']
  list_filter = ('user',)
  search_fields = ['user__first_name', 'user__last_name']
  list_per_page = 25

admin.site.register(EngageGroups, CustomEngageGroups)
admin.site.register(GroupPerformance, CustomGroupPerformance)
admin.site.register(ReportUser, CustomReportUser)
admin.site.register(UserPosts, CustomUserPosts)
admin.site.register(GroupPosts, CustomGroupPosts)
admin.site.register(UserPostsComments, CustomUserPostsComments)
admin.site.register(GroupPostsComments, CustomGroupPostsComments)
admin.site.register(About, CustomAbout)
admin.site.register(Moderation, CustomModeration)
admin.site.register(SadbhavanaScore, CustomSadbhavanaScore)
