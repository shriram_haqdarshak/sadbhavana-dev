from django.urls import path, include
from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve


urlpatterns = [
    url(r'^$', views.gethome, name='home-engage'),
    path('home', views.gethome, name='home-engage'),
    path('profile', views.getprofile, name='profile-engage'),
    path('groups', views.getgroups, name='groups-engage'),
    path('my-groups', views.getmygroups, name='my-groups-engage'),
    path('posts-view/<int:post_id>/', views.singlepost, name='posts-view'),
    path('posts-view/<int:post_id>/<str:not_type>/<int:not_id>', views.singlepost, name='posts-view'),
    path('friends', views.allFriends, name='friends'),
    path('group-view/<int:header_id>', views.singlegroup, name='group-view'),
    path('group-create', views.creategroup, name='group-create'),
    path('about-engage', views.aboutengage, name='about-engage'),
    path('user/<int:header_id>', views.userprofile, name='user'),
    path('user/undefined', views.usernotfound, name='user-not-found'),
    path('group-posts-view/<int:post_id>', views.singlegrouppost, name='group-posts-view'),
    path('group-posts-view/<int:post_id>/<str:not_type>/<int:not_id>', views.singlegrouppost, name='group-posts-view'),
    path('group-settings/<int:group_id>', views.groupsettings, name='group-settings'),
    path('competition-winners', views.competitionwinners, name='competition-winners'),
    path('view-competition-winners/<int:comp_id>', views.singlecompetitionwinner, name='view-competition-winners'),


    url(r'insert$', views.insert, name='insert'),
    url(r'image_upload_ajax$', views.image_upload_ajax, name='image_upload_ajax'),
    url(r'unfriend$', views.unfriend, name='unfriend'),
    url(r'acceptRequest$', views.acceptRequest, name='acceptRequest'),
    url(r'addfriend$', views.addfriend, name='addfriend'),
    url(r'cancelrequest$', views.cancelrequest, name='cancelrequest'),
    url(r'creategroupajax$', views.creategroupajax, name='creategroupajax'),
    url(r'joingroup$', views.joingroup, name='joingroup'),
    url(r'leaveGroup$', views.leaveGroup, name='leaveGroup'),
    url(r'createpostajax$', views.createpostajax, name='createpostajax'),
    url(r'creategrouppostajax$', views.creategrouppostajax, name='creategrouppostajax'),
    url(r'commentuserpostajax$', views.commentuserpostajax, name='commentuserpostajax'),
    url(r'likeuserpostajax$', views.likeuserpostajax, name='likeuserpostajax'),
    url(r'commentgrouppostajax$', views.commentgrouppostajax, name='commentgrouppostajax'),
    url(r'likegrouppostajax$', views.likegrouppostajax, name='likegrouppostajax'),
    url(r'changegroupimage$', views.changegroupimage, name='changegroupimage'),
    url(r'deleteusercomment$', views.deleteusercomment, name='deleteusercomment'),
    url(r'deletegroupcomment$', views.deletegroupcomment, name='deletegroupcomment'),
    url(r'deleteuserpost$', views.deleteuserpost, name='deleteuserpost'),
    url(r'deletegrouppost$', views.deletegrouppost, name='deletegrouppost'),
    url(r'addsadbhavanapoints$', views.addsadbhavanapoints, name='addsadbhavanapoints'),
    url(r'reportuser$', views.reportuser, name='reportuser'),
    url(r'deletegroup$', views.deletegroup, name='deletegroup'),
    url(r'changePrivacy$', views.changePrivacy, name='changePrivacy'),
    url(r'getLikesAjax$', views.getLikesAjax, name='getLikesAjax'),
    url(r'getGroupPostLikesAjax$', views.getGroupPostLikesAjax, name='getGroupPostLikesAjax'),

    url(r'media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
]