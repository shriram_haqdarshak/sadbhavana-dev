from django.conf import settings
from .models import Friends, About, GroupMembers, EngageGroups, SadbhavanaScore, UserPosts, GroupPostsLikes, GroupPostsComments
from app.models import User
from django.utils import translation

def get_settings(context):
  return {'GATrackingId': settings.GA_TRACKING_ID}

# Get user friend list
def get_friends_list(request):

  user_id = request.user.id
  friendsList = []

  if user_id :
    a = """ SELECT requester_id FROM engage_friends WHERE responser= {} """.format(user_id)

    q = """ SELECT *, au.id as friend_id FROM app_user au
            LEFT JOIN engage_about ea ON au.id=ea.user_id
            WHERE au.id IN({}) LIMIT 5""".format(a)

    friendsList1 = User.objects.raw(q)

    b = """ SELECT responser FROM engage_friends WHERE requester_id= {} """.format(user_id)

    r = """ SELECT *, au.id as friend_id FROM app_user au
            LEFT JOIN engage_about ea ON au.id=ea.user_id
            WHERE au.id IN({}) LIMIT 5""".format(b)

    friendsList2 = User.objects.raw(r)

    for x in friendsList1:
      friendsList.append(x)

    for x in friendsList2:
      friendsList.append(x)

    d = """ SELECT id, first_name, last_name FROM app_user """.format()
    allmembers = User.objects.raw(d)

    e = """ SELECT id, name FROM engage_engagegroups WHERE moderater_status = 'verified' LIMIT 5 """.format()
    allgroups = EngageGroups.objects.raw(e)

    f = """ SELECT * FROM engage_sadbhavanascore WHERE user_id = {} """.format(user_id)
    sadbhavanascore = SadbhavanaScore.objects.raw(f)

    return {
      'baseURL': settings.BASE_URL,
      'friendsList': friendsList,
      'allmembers': allmembers,
      'allgroups': allgroups,
      'sadbhavanascore': sadbhavanascore
    }
  else :
    return {
      'baseURL': settings.BASE_URL,
      'friendsList': '',
      'allmembers': '',
      'allgroups': '',
      'sadbhavanascore': ''
  }

# get user joined group list
def get_groups_list(request):

  user_id = request.user.id

  if user_id :
    q = """ SELECT * FROM engage_groupmembers egm
            JOIN engage_engagegroups eeg ON egm.group_id = eeg.id
            WHERE egm.user_id = {} AND eeg.moderater_status = 'verified' LIMIT 5 """.format(user_id)

    grouplistdetails = GroupMembers.objects.raw(q)

    if user_id:
      x = """ SELECT * FROM app_user WHERE id= {} """.format(user_id)
      userdetails = User.objects.raw(x)
      return {
        'baseURL': settings.BASE_URL,
        'grouplistdetails': grouplistdetails,
        'userdetails': userdetails[0]
      }
    else:
      return {
        'baseURL': settings.BASE_URL,
        'grouplistdetails': grouplistdetails
      }
  else :
    return {
        'baseURL': settings.BASE_URL,
        'grouplistdetails': '',
        'userdetails': ''
      }

# Get notification when another user comment/like user's post or send friend request
def get_notifications(request):

  user_id = request.user.id
  notificationCount = 0

  if user_id :
    r = """ SELECT epc.id,epc.post_id,epc.user_id as commenter_id,au.first_name,au.last_name FROM engage_userpostscomments epc
            JOIN engage_userposts eup ON eup.id=epc.post_id
            JOIN app_user au on au.id = epc.user_id
            WHERE eup.user_id = {} AND epc.moderater_status = 'verified' AND epc.is_read= 'no' AND epc.user_id != {} """.format(user_id,user_id)
    Notifcomments = UserPosts.objects.raw(r)
    commentscount = sum(1 for comments in Notifcomments)

    s = """ SELECT epl.id,epl.post_id,epl.user_id as liker_id,au.first_name,au.last_name FROM engage_userpostslikes epl
            JOIN engage_userposts eup ON eup.id=epl.post_id
            JOIN app_user au on au.id = epl.user_id
            WHERE eup.user_id = {} AND epl.is_read= 'no' AND epl.user_id != {} """.format(user_id,user_id)
    Notiflikes = UserPosts.objects.raw(s)
    likescount = sum(1 for likes in Notiflikes)

    s = """ SELECT ef.id,au.first_name,au.last_name FROM engage_friends ef
            JOIN app_user au on au.id = ef.requester_id
            WHERE ef.responser = {} AND ef.request_status= 'Pending' """.format(user_id)
    Notifreq = UserPosts.objects.raw(s)
    reqcount = sum(1 for likes in Notifreq)

    w = """ SELECT epl.id,epl.post_id, epl.user_id as liker_id, au.first_name,au.last_name, eg.name as group_name FROM engage_grouppostslikes epl
        JOIN engage_groupposts egp ON egp.id=epl.post_id
        JOIN engage_engagegroups eg ON eg.id=egp.group_id
        JOIN app_user au ON au.id=epl.user_id
        WHERE egp.user_id = {} AND epl.is_read= 'no' AND epl.user_id != {} """.format(user_id,user_id)
    Notifgrouppostlikes = GroupPostsLikes.objects.raw(w)
    grouppostlikescount = sum(1 for likes in Notifgrouppostlikes)

    r = """ SELECT egpc.id,egpc.post_id,egpc.user_id as commenter_id,au.first_name,au.last_name, eg.name as group_name FROM engage_grouppostscomments egpc
            JOIN engage_groupposts egp ON egp.id=egpc.post_id
            JOIN engage_engagegroups eg ON eg.id=egp.group_id
            JOIN app_user au on au.id = egpc.user_id
            WHERE egp.user_id = {} AND egpc.moderater_status = 'verified' AND egpc.is_read= 'no' AND egpc.user_id != {} """.format(user_id,user_id)
    Notifgrouppostcomments = GroupPostsComments.objects.raw(r)
    grouppostcommentscount = sum(1 for comments in Notifgrouppostcomments)

    notificationCount = commentscount + likescount + reqcount + grouppostlikescount + grouppostcommentscount

    return {
      'Notifcomments': Notifcomments,
      'Notiflikes' : Notiflikes,
      'Notifreq' : Notifreq,
      'Notifgrouppostlikes' : Notifgrouppostlikes,
      'Notifgrouppostcomments' : Notifgrouppostcomments,
      'notificationCount':notificationCount
    }
  else :
    return {
      'Notifcomments': '',
      'Notiflikes' : '',
      'notificationCount' : notificationCount
    }